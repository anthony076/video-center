import sys
import ujson
import asyncio
import base64

from aiohttp import web
from multidict import MultiDictProxy
from urllib.parse import unquote

from utils.db import db
from utils.fileUtil import *
from utils.download import manager, startWorker
from utils.logger import loggers
from config.const import Const

mainLogger = loggers["main"].createLogger()
routes = web.RouteTableDef()

# ========================
# middleware & Signal
# ========================
@web.middleware
async def logReq(req, handler):
    mainLogger.info(f"req-url = {req.url}")

    resp = await handler(req)
    return resp

async def addCorsHeader(req, resp):
    resp.headers["Access-Control-Allow-Origin"] = "*"
    resp.headers["Access-Control-Allow-Credentials"] = "true"
    resp.headers["Access-Control-Allow-Headers"] = "authorization"
    resp.headers["Access-Control-Allow-Methods"] = "OPTIONS,HEAD,GET,PUT,POST,DELETE,PATCH"
    
# ========================
# Web manager interface
# ========================
@routes.get("/")
async def home(request) -> web.Response:
    with open("html/index.html", "rb") as f:
        content = f.read().decode("utf-8")
    return web.Response(text=content, content_type='text/html')

# ========================
# 取回影片列表
# http://127.0.0.1:5566/list?dst=aa
# ========================
@routes.get("/list")
async def listVideo(request) -> web.Response:
    params: MultiDictProxy = request.rel_url.query

    dst = params.get("dst", None)
    videos = ujson.dumps({"videos": db.all(dst)}, ensure_ascii=False)
    return web.Response(text=videos)

# ========================
# 撥放影片
# http://127.0.0.1:5566/v?id=d405fb085cdffb6968f3e6297568821d
# ========================
@routes.get("/v")
async def getVideo(request) -> web.Response:
    params: MultiDictProxy = request.rel_url.query
    md5 = params.get("id", None)

    if md5:
        return playVideo(md5)
    return web.json_response(Const.JSON_MISS_Parameter)

# ========================
# 刪除影片
# http://127.0.0.1:5566/r?id=97430f319fb9638218bc59b36d473060
# http://127.0.0.1:5566/r?all=e72bf6080df799ac824811afb346dac1,d1e0ba4aa8df58e366e2fbdc48df279f,d405fb085cdffb6968f3e6297568821d
# ========================
@routes.get("/r")
async def removeVideo(request) -> web.Response:
    params: MultiDictProxy = request.rel_url.query

    md5 = params.get("id", None)
    strList = params.get("all", None)

    # id 參數和 all 參數不能同時提供
    if md5 and strList:
        return web.Response(text="error parameter")

    # 只有 id 參數
    if md5:
        return delOneVideo(md5=md5)

    # 只有 all 參數
    if strList:
        md5list = strList.split(",")
        return delAllVideo(md5list=md5list)

    # id 參數和 all 參數都沒有提供
    return web.json_response(Const.JSON_MISS_Parameter)

# ========================
# 移動影片
# http://127.0.0.1:5566/mv?ids=97430f319fb9638218bc59b36d473060&dst=bb
# ========================
@routes.get("/mv")
async def moveVideo(request) -> web.Response:
    params: MultiDictProxy = request.rel_url.query

    md5s = params.get("ids", None)
    dst = params.get("dst", None)

    # ids 參數 必須 dst 參數必須同時提供
    if md5s and dst:
        md5List = md5s.split(",")
        return moveVideos(md5List, dst)
    else:
        return web.json_response(Const.JSON_MISS_Parameter)

# ========================
# 返回所有有效的資料夾名稱
# http://127.0.0.1:5566/lf
# ========================
@routes.get("/lf")
async def validFolder(request) -> web.Response:
    return listFolder()

# ========================
# 建立資料夾
# http://127.0.0.1:5566/mf?name=dd
# ========================
@routes.get("/mf")
async def createFolder(request) -> web.Response:
    params: MultiDictProxy = request.rel_url.query
    folderName = params.get("name", None)

    if folderName:
        return makeFolder(folderName)

    # return web.Response(text="missing parameter", headers=setting.corsHeader)
    return web.json_response(Const.JSON_MISS_Parameter)

# ========================
# 下載影片
# y2b
# 單一影片，http://127.0.0.1:5566/vd?u=aHR0cHM6Ly93d3cueW91dHViZS5jb20vd2F0Y2g/dj11SWtpdVlZYjhETSZsaXN0PVBMcXBOLWdTUkVIWHdZUklzanc4UW1oMFFqSkhfN1p3eEsmaW5kZXg9Mjg=
# 指定影片，http://127.0.0.1:5566/vd?pl=1&pis=1-3,5,7&u=aHR0cHM6Ly93d3cueW91dHViZS5jb20vd2F0Y2g/dj11SWtpdVlZYjhETSZsaXN0PVBMcXBOLWdTUkVIWHdZUklzanc4UW1oMFFqSkhfN1p3eEsmaW5kZXg9Mjg=
# 整個列表，http://127.0.0.1:5566/vd?pl=1&u=aHR0cHM6Ly93d3cueW91dHViZS5jb20vd2F0Y2g/dj11SWtpdVlZYjhETSZsaXN0PVBMcXBOLWdTUkVIWHdZUklzanc4UW1oMFFqSkhfN1p3eEsmaW5kZXg9Mjg=

# bilibili
# 單一影片，http://127.0.0.1:5566/vd?u=aHR0cHM6Ly93d3cuYmlsaWJpbGkuY29tL3ZpZGVvL0JWMTFoNDExbTdaRT9zcG1faWRfZnJvbT0zMzMuODUxLmJfNzI2NTYzNmY2ZDZkNjU2ZTY0LjI=
# 指定影片，http://127.0.0.1:5566/vd?pl=1&pis=1-3&u=aHR0cHM6Ly93d3cuYmlsaWJpbGkuY29tL3ZpZGVvL0JWMUpONDExUjd3Vz9mcm9tPXNlYXJjaCZzZWlkPTI3NjIyNjczMjY4NjY3NDgxOA==
# 整個列表，http://127.0.0.1:5566/vd?pl=1&u=aHR0cHM6Ly93d3cuYmlsaWJpbGkuY29tL3ZpZGVvL0JWMUpONDExUjd3Vz9mcm9tPXNlYXJjaCZzZWlkPTI3NjIyNjczMjY4NjY3NDgxOA==
# space*2，http://127.0.0.1:5566/vd?pl=1&u=aHR0cHM6Ly9zcGFjZS5iaWxpYmlsaS5jb20vNTA1Njk5NjMyP3NwbV9pZF9mcm9tPTMzMy43ODguYl83NjVmNzU3MDY5NmU2NjZmLjI=
# space*192，http://127.0.0.1:5566/vd?pl=1&u=aHR0cHM6Ly9zcGFjZS5iaWxpYmlsaS5jb20vNDgzMTYyNDk2P3NwbV9pZF9mcm9tPTMzMy43ODguYl83NjVmNzU3MDY5NmU2NjZmLjI=

# gimy.app
# 單一影片，http://127.0.0.1:5566/vd?u=aHR0cHM6Ly9naW15LmFwcC9lcHMvMTQ4OTM5LTEtMS5odG1s
# 單一影片，http://127.0.0.1:5566/vd?u=aHR0cHM6Ly9naW15LmFwcC9lcHMvMTQ4OTM5LTEtMi5odG1s
# 影片列表，http://127.0.0.1:5566/vd?u=aHR0cHM6Ly9naW15LmFwcC92b2QvMTQ2MzcwLmh0bWw=
# 影片列表，http://127.0.0.1:5566/vd?u=aHR0cHM6Ly9naW15LmFwcC92b2QvMTQ4OTM5Lmh0bWw=

# gimytv
# 電影(介紹頁)，http://127.0.0.1:5566/vd?u=aHR0cHM6Ly9naW15dHYuY29tL3ZfN0pOLmh0bWw=
# 電影(播放頁)，http://127.0.0.1:5566/vd?u=aHR0cHM6Ly9naW15dHYuY29tL2VwLTdKTi0xLTEuaHRtbA==
# 電視劇(介紹頁)，http://127.0.0.1:5566/vd?u=aHR0cHM6Ly9naW15dHYuY29tL3ZfaDM2Lmh0bWw=
# ========================
@routes.get("/vd")
async def download(request) -> web.Response:
    querys: MultiDictProxy = request.rel_url.query
    url = querys.get("u", None)

    if url:
        params: dict = {}
        for key, value in querys.items():
            params[key] = value

        # 把base64的網址還原
        params["u"] = base64.b64decode(url).decode("utf-8")

        return downloadVideo(params)
    else:
        return web.json_response(Const.JSON_MISS_Parameter)

# ========================
# 檢視下載狀態
# http://127.0.0.1:5566/tstatus
# ========================
# ! 使用 queue ，正在下載的任務無法取得，因為該任務已經被取出
@routes.get("/tstatus")
async def get_download_status(request) -> web.Response:
    return downloadStatus()

# ========================
# 重新掃描資料夾
# http://127.0.0.1:5566/scan
# ========================
@routes.get("/scan")
async def scan(request) -> web.Response:
    return scanFolder()

# ========================
# 回復 server 狀態
# ========================
@routes.get("/ping")
async def ping(request) -> web.Response:
    return web.Response(text="pong")

# ========================
# rename，單純對檔案進行更名，不涉及資料庫的操作
# @old: 更名前的檔案路徑
# @new: 更名後的檔案路徑
# http://127.0.0.1:5566/rename?old=videos/144178-1-3.ts&new=videos/%E6%BA%AB%E5%B7%9E%E4%B8%80%E5%AE%B6%E4%BA%BA-3.ts
# ========================
@routes.get("/rename")
async def _rename(request) -> web.Response:
    querys: MultiDictProxy = request.rel_url.query
    
    oldPath = querys.get("old", None)
    if oldPath:
        oldPath = unquote(oldPath)
    else:
        return web.json_response(Const.JSON_MISS_Parameter)

    newPath = querys.get("new", None)
    if newPath:
        newPath = newPath.replace("@", "%")
        newPath = unquote(newPath)
    else:
        return web.json_response(Const.JSON_MISS_Parameter)

    return renameFile(oldPath=oldPath, newPath=newPath)

# ========================
# 關閉 server
# ========================
@routes.get("/quit")
async def quit(request) -> None:
    # ==== 關閉背景線程 ====
    # 關閉不同線程的 event-loop 不能直接調用 loop.close()
    # manager.getloop.close()

    # 必須透過 call_soon_threadsafe()
    backloop = manager.backloop
    backloop.call_soon_threadsafe(backloop.stop)

    # ==== 離開程式 ====
    sys.exit()

async def run_server():
    app = web.Application(middlewares=[logReq])
    app.on_response_prepare.append(addCorsHeader)

    app.add_routes(routes)

    # 建立網站內容執行器
    # access_log=False，避免內建的 logger 輸出
    runner = web.AppRunner(app, access_log=False)
    await runner.setup()

    # 啟動網站
    site = web.TCPSite(runner, port="5566")
    await site.start()
    print("==== server @ http://127.0.0.1:5566 ====")

# ========================
# 流程
# ========================
# 程式執行時，run_server() 和 startWorker() 兩個異步函數會同時啟動，只有server結束才會停止
#   -> server 接收到下載請求後 -> 透過 DownloadManager 建立新的 task -> 將 task 加入 queue 後回到 ui-thread (非阻塞)
#       -> 預設的 startWorker() 會建立 5 個 worker，每個 woker 都會進入無限循環，並持續從 DownloadManager._quque 中取得下載任務並執行
async def main():
    await asyncio.gather(
        run_server(),
        startWorker()
    )

if __name__ == "__main__":
    asyncio.run(main())