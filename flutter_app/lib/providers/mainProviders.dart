
import 'package:flutter_riverpod/flutter_riverpod.dart';

final moveToProvider = StateProvider<String> ((ref) => "videos");
final pageIndexProvider = StateProvider<int> ((ref) => 1);
final isSelectModeProvider = StateProvider<bool> ((ref) => false);