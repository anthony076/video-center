import 'package:flutter/material.dart';
import 'package:chewie/chewie.dart';
import 'package:video_player/video_player.dart';

class PlayerPage extends StatefulWidget {
  final Key key;
  final String url;
  final String title;
  
  PlayerPage({this.key, this.url, this.title = "Video Player"}): super(key: key);

  @override
  _PlayerPageState createState() => _PlayerPageState();
}

class _PlayerPageState extends State<PlayerPage> {

  // 建立影片控制器
  static VideoPlayerController _videoPlayerController;
  // 建立面板控制器
  static ChewieController _chewieController;

  _PlayerPageState();

  @override
  void initState(){
    super.initState();
    
    // 建立影片播放控制器
    _videoPlayerController = VideoPlayerController.network(widget.url);

    // 建立面板控制器
    _chewieController = ChewieController(
      fullScreenByDefault: true,
      videoPlayerController: _videoPlayerController,
      autoPlay: true,
      // looping: true,
      showControls: true,
      // autoInitialize=true 會將第一禎畫面當作封面
      // autoInitialize: true,
      // 處理錯誤
      errorBuilder: (context, errorMessage) {
        return Center(
          child: Text(errorMessage, style: TextStyle(color: Colors.white)),
        );
      }
    );
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Chewie(controller: _chewieController)
    );
  }

  @override
  void dispose() {
    _chewieController?.dispose();
    _videoPlayerController.dispose();
    super.dispose();
  }
}
