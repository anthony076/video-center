import os
from peewee import *
from base64 import b64encode
from hashlib import md5
from pathlib import Path, WindowsPath
from typing import List, Optional

from config.config import setting
from utils.logger import loggers

dbLogger = loggers["db"].createLogger()
sql = SqliteDatabase('video.db')

class Video(Model):
    name = TextField()
    md5 = TextField()
    b64 = TextField()
    location = TextField()

    class Meta:
        database = sql  # This model uses the "people.db" database.

class Handler:
    def __init__(self):
        self.model = Video
        self.model.create_table([self.model])

    @staticmethod
    def name2b64(filename: str) -> str:
        return b64encode(filename.encode("utf-8")).decode("utf-8").replace("/", "_").replace("+", "-")

    @staticmethod
    def name2md5(filename: str) -> str:
        m = md5()
        m.update(filename)
        return m.hexdigest()

    def scan(self):
        """
        Scan video folder, and add the file that not listed in db.
        # .ts .mp4 .mkv .flv
        """
        
        #videoFiles: List[WindowsPath] = list(item for item in Path( f"./{setting.videoFolderName}").glob("**/*") if not item.is_dir())
        videoFiles: List[WindowsPath] = list(p for p in Path(f"./{setting.videoFolderName}").glob("**/*") if p.suffix in [".ts", ".mp4", ".mkv", ".flv", "webm"])
        
        # ==== 將新增檔案添加到資料庫中 ====
        for path in videoFiles:
            print("=" * 30)

            location = path.parent
            fileName = path.name

            b64 = Handler.name2b64(fileName)
            md5 = Handler.name2md5(fileName.encode("utf-8"))

            self.add(fileName, b64, md5, location)
        
        # ===== 檢驗資料庫中的檔案是否有效，若失效就移除 =====
        allFiles = self._allFile()
        for fileDict in allFiles:
            
            targetFile = fileDict["path"] + "/" + fileDict["name"]
            if not os.path.isfile(targetFile):
                self.deleteVideo(fileDict["md5"])

    def isExisted(self, md5: str) -> bool:
        return self.model.select().where(self.model.md5 == md5).exists()

    # add new record into db sync
    def add(self, fileName: str, b64: str, md5: str, location: str) -> None:
        if not self.isExisted(md5):
            # 資料庫不存在該檔案
            self.model.create(
                name=fileName,
                b64=b64,
                md5=md5,
                location=location
            ).save()

            dbLogger.info(f"added {fileName}")
        else:
            # 資料庫已存在該檔案
            dbLogger.info(f"skip, {fileName} is already existed")

    # update newLocation via md5
    def updataPath(self, md5: str, newLocation: str):
        Video.update(location=newLocation).where(Video.md5 == md5).execute()

    def _allFile(self) -> list[dict]:
        result: list = []

        for row in Video.select(Video.name, Video.md5, Video.location):
            result.append({"name": row.name, "md5": row.md5, "path": row.location})

        return result

    # list all record for public-api
    def all(self, dst=None) -> list:
        result: list = []
        folderPath: str = "videos"

        if dst:
            folderPath += f"\\{dst}"

        for row in Video.select(Video.name, Video.md5).where(Video.location == folderPath):
            result.append({"name": row.name, "md5": row.md5})

        return result

    # get filename、filepath、full-path via md5
    def getFile(self, md5: str) -> Optional[dict]:
        try:
            row = Video.select(Video.location, Video.name).where(
                Video.md5 == md5).get()
            return {
                "path": row.location,
                "name": row.name,
                "full": row.location + f"\\{row.name}"}

        except Exception:
            return None

    # delete a video record by md5
    def deleteVideo(self, md5) -> str:
        if self.isExisted(md5):
            row = Video.get(Video.md5 == md5)
            deleted = row.delete_instance()

            if deleted > 0:
                print(f"delete {row.name} success")
                return True
            else:
                print(f"delete {row.name} fail")
                return False
        else:
            return False

db = Handler()
db.scan()
