
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_app/providers/commonProviders.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class IpComponent extends ConsumerWidget {
  
  // 焦點控制
  final FocusNode ipAnode = FocusNode();
  final FocusNode ipBnode = FocusNode();
  final FocusNode ipCnode = FocusNode();
  final FocusNode ipDnode = FocusNode();

  // 輸入文字控制
  final TextEditingController ipACtl = TextEditingController();
  final TextEditingController ipBCtl = TextEditingController();
  final TextEditingController ipCCtl = TextEditingController();
  final TextEditingController ipDCtl = TextEditingController();

  // 產生輸入框函數
  TextField _genTextField(context, controller, preFnode, curFnode, nextFnode, maxLen, setReqUrl){
    return TextField(
      textAlign: TextAlign.center,
      textInputAction: TextInputAction.done,
      keyboardType: TextInputType.number,
      controller: controller,
      inputFormatters: [
        LengthLimitingTextInputFormatter(maxLen)
      ],

      onChanged: (text){
        if(text.length >= 3){
          FocusScope.of(context).requestFocus(nextFnode);
        } else if (text.length == 0) {
          FocusScope.of(context).requestFocus(preFnode);
        } else {}
        
        // 注意，一定要加上 http://<ip>:port，只有IP會連接失敗
        setReqUrl("http://${ipACtl.text}.${ipBCtl.text}.${ipCCtl.text}.${ipDCtl.text}:5566");
      },

      focusNode: curFnode,
    );
  }

  // 產生分隔文字函數
  Widget _genSepText(text){
    return Text(
      text, 
      style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
      textAlign: TextAlign.end,
    );
  }

  @override
  Widget build(BuildContext context, ScopedReader watch){
    void Function(String) setReqUrl = context.read(urlProvider).setReqUrl;

    return GestureDetector(
      child: Row(
        children: <Widget>[
          Container(
            child: _genTextField(context, ipACtl, null, ipAnode, ipBnode, 3, setReqUrl),
            width: 30,
            height: 60,
          ),
          _genSepText("."),
          Container(
            child: _genTextField(context, ipBCtl, ipAnode, ipBnode, ipCnode, 3, setReqUrl),
            width: 30,
            height: 60,
          ),
          _genSepText("."),
          Container(
            child: _genTextField(context, ipCCtl, ipBnode, ipCnode, ipDnode, 3, setReqUrl),
            width: 30,
            height: 60,
          ),
          _genSepText("."),
          Container(
            child: _genTextField(context, ipDCtl, ipCnode, ipDnode, null, 3, setReqUrl),
            width: 30,
            height: 60,
          ),
        ],
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      ),
      onTap: (){
        FocusScope.of(context).unfocus();
      },
    );
  }
}
