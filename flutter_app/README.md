## 架構
CheckPage，初始路由，檢查 Server 是否上線
    -> MainPage，建立主頁面框架，建構 Buttom-Navigation-Bar，用於顯示遠端目錄、影片列表、下載三個頁面
        -> SelectPage，用來選擇影片目錄
        -> ListPage，建構列表的主框架，底部導航列預設畫面
            -> ItemView，用於顯示列表的子項目
            -> playerPage，用於撥放影片
        -> DownloadPage，建構下載頁面
    
## 注意事項
- Flutter 環境
    Flutter (Channel beta, 1.26.0-17.6.pre, on Microsoft Windows [Version 10.0.17763.107], locale zh-TW)

- 注意，測試播放遠方 server 的影片時
    不能在虛擬機中使用 localhost 或 192.168.x.x，虛擬機和本機是不同的網域
    解法1，虛擬機可以將 127.0.0.1 替換成 10.0.2.2，10.0.2.2 是虛擬機用來連接本機 server 的網址
    解法2，不利用虛擬機而是利用真機進行測試，並將真機和本機連接到同一個 wifi，如此，可使用同一個 192.168.x.x 的 IP

    參考，
    https://developer.android.com/studio/run/emulator-networking
    https://medium.com/@podcoder/connecting-flutter-application-to-localhost-a1022df63130

- issue，使用模擬器播放影片會遇到 crash，但手動執行 app 是正常的
  解決方式，將執行改成 flutter run，不要使用 flutter run --enable-software-rendering

## 測試
- step1，複製專案資料夾，切換到 server-branch，並將資料夾命名為 server
- step2，將 video 目錄放在桌面上
- step3，啟動模擬器
- step4，執行 app-branch 的 run_test.bat

## issue 
- Cannot fit requested classes in a single dex file (# methods: 66370 > 65536)
    專案中引用的庫太多，使得方法數超過 65536
    https://learnku.com/articles/33548

## TODO
- 實現 目錄頁面的刷新
- 實現 影片頁面的刷新
- 點擊下載頁面後，路由跳轉，隱藏 appBar 和 Bottom-navigator-bar
- 出現支援的網址時才將下載按鈕致能
- 記住最後一次瀏覽位置
- 符合的網址，按鈕高亮，反則反白
- 打開 app 後，掃描可用 內網IP (IP設置頁)