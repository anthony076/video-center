
from pathlib import Path
from abc import ABC, abstractmethod
from typing import Optional

class ParserBase (ABC):
    pwd = Path.cwd()

    @abstractmethod
    def _genArgs(self, params: dict):
        """ transform url parameter into command args """
        pass

    @abstractmethod
    def download(self, params: dict):
        """ exeute download process """
        pass

class GimyBase(ABC):
    # 輸入合集頁面，找出合集頁面中的所有的分集頁面
    # 輸入所有的分集頁面，
    #   尋找 m3u8 下載網址，得到 m3u8Url、videoName、vid
    #   建立 cmd 命令
    #   將 cmd 命令寫入 batch 檔案中
    header = {"user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.129 Safari/537.36"}

    @abstractmethod
    def _fetch(self, url: str) -> Optional[str]:
        # try:
        #     request = Request(url, headers=GimyBase.header)
        #     res = urlopen(request)
        # except Exception as e:
        #     print(f"[Download][fetch] Err: {e}")
        #     return None
        # return res.read().decode("utf-8")
        
        # ===== aiohttp ====
        # timeout = aiohttp.ClientTimeout(total=20)
        # async with aiohttp.ClientSession(timeout=timeout) as sess:
        #     try:
        #         async with sess.get(url) as resp:
        #             content = await resp.text()
        #             return content

        #     except asyncio.exceptions.TimeoutError:
        #         print(f"[fetch] request timeout @ {url}")

        # ===== request2 ====
        # content = ""
        # try:
        #     sess = requests.session()
        #     resp = sess.get(url, timeout=20)

        #     if resp.status_code != 200:
        #         print(f"[fetch] code:{resp.status_code}, can't fetch content")
        #     else:
        #         content = resp.text
        # except requests.exceptions.ReadTimeout:
        #     print(f"[fetch] timeout @ {url}")
        
        # return content
        pass

    @abstractmethod
    def _pid(self, collectPageUrl: str) -> int:
        """
        page-id,
        https://gimytv.com/v_<pid>.html
        https://gimy.app/vod/<pid>.html
        """
        pass
    
    @abstractmethod
    def _m3u8Url(self, epPageUrl: str) -> list:
        pass

    @abstractmethod
    def _videoName(self, epPageUrl: str) -> str:
        pass

    @abstractmethod
    def _epPageUrls(self, collectPageUrl: str) -> list:
        pass
