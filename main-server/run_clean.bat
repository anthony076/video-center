@echo off

del /F /Q m3u8-*.bat 
del /F /Q /S video.db log.html report.html output.xml
rmdir /S /Q __pycache__ videos tools Downloads