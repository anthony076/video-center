import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/providers/commonProviders.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final downloadProvider =
    ChangeNotifierProvider.autoDispose((ref) => DownloadStore(ref));

class DownloadStore extends ChangeNotifier {
  // ==============
  // 狀態
  // ==============
  AutoDisposeProviderReference ref;
  GlobalKey webViewKey = GlobalKey();
  //String currentUrl = "";
  String currentDropItem = "youtube";
  List<String> tids = [];

  InAppWebViewController webViewController;

  InAppWebViewGroupOptions webviewOptions = InAppWebViewGroupOptions(
      crossPlatform: InAppWebViewOptions(
        useShouldOverrideUrlLoading: true,
        mediaPlaybackRequiresUserGesture: false,
      ),
      android: AndroidInAppWebViewOptions(
        useHybridComposition: true,
      ));

  // ==============
  // 構造
  // ==============
  DownloadStore(this.ref);

  // ==============
  // 方法
  // ==============
  static Map siteMap = {
    "youtube" : {
      "url" : "https://www.youtube.com",
      "supportBatch" : true,
      "supportSelect" : true,
    },

    "bilibili" : {
      "url" : "https://www.bilibili.com/",
      "supportBatch" : true,
      "supportSelect" : true,
    },

    "gimy" : {
      "url" : "https://gimy.app/",
      "supportBatch" : true,
      "supportSelect" : false
    },
  };

  // String siteMap(String name) {
  //   String url;
  //   switch (name) {
  //     case "youtube":
  //       url = "https://www.youtube.com";
  //       break;
  //     case "bilibili":
  //       url = "https://www.bilibili.com/";
  //       break;
  //     case "gimy":
  //       url = "https://gimy.app/";
  //       break;
  //   }

  //   //currentUrl = url;
  //   return url;
  // }

  // 網站選擇鈕狀態變更後，用於更新 drop-down 鈕
  void setSite(newSite) {
    currentDropItem = newSite;
    notifyListeners();
  }

  String _btoa(String url){
    var bytes = utf8.encode(url);
    var base64Str = base64.encode(bytes);
    return base64Str;
  }

  // 發出下載請求
  Future<void> send({String url, bool isList=false, String select=""}) async{
      String reqUrl = ref.read(urlProvider).reqUrl;
      Response resp;
      Dio dio = ref.read(diolProvider(10000));

      List<String> params = [];
      String downloadUrl;
      String b64Url;

      if (isList){
        params.add("pl=1");
      } 

      if (select != "") params.add("pis=$select");
      
      b64Url = _btoa(url);
      downloadUrl = reqUrl + "/vd?" + params.join('&') + "&u=$b64Url";
      print(downloadUrl);

      resp = await dio.get(downloadUrl); 
      tids.add(resp.data["tid"]);      

  }
}
