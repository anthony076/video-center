import 'package:flutter/material.dart';

TextStyle msgStyle(String color, double textSize) {
  Color msgColor;

  switch (color) {
    case "black":
      msgColor = Colors.black;
      break;
      
    case "blue":
      msgColor = Colors.blue;
      break;

    case "red":
      msgColor = Colors.red;
      break;

    case "white":
      msgColor = Colors.white;
      break;

    default:
      msgColor = Colors.black;
  }

  return TextStyle(
    color: msgColor,
    fontSize: textSize,
  );
}

