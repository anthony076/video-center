import os
import wget
import shutil

class Setting:
    cwd: str = os.getcwd()
    videoFolderName: str = "videos"
    corsHeader: dict = {
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Credentials": "true",
        "Access-Control-Allow-Headers": "authorization",
        "Access-Control-Allow-Methods": "OPTIONS,HEAD,GET,PUT,POST,DELETE,PATCH",
    }

    def __init__(self):
        self._checkFolders()
        self._checkExe()

    def _checkFolders(self) -> None:
        folders = [
            "videos", "tools"
        ]

        for folder in folders:
            if not os.path.isdir(folder):
                os.mkdir(folder)

    def _getExe(self, exeInfo: dict) -> None:
        if exeInfo["name"] == "youtubedl":
            print("download youtubedl ...")
            wget.download(exeInfo["link"], out="tools\\youtubedl.exe")
            print("\n")

        if exeInfo["name"] == "annie":
            print("download annie ...")
            wget.download(exeInfo["link"], out="tools\\annie.zip")
            shutil.unpack_archive("tools\\annie.zip", "tools")
            os.remove("tools\\annie.zip")
            print("\n")

        if exeInfo["name"] == "n_m3u8dl":
            print("download N_m3u8 ...")
            wget.download(exeInfo["link"], out="tools\\myng.exe")
            print("\n")

        if exeInfo["name"] == "curl":
            print("download curl ...")
            wget.download(exeInfo["link"], out="tools\\mycurl.zip")
            shutil.unpack_archive("tools\\mycurl.zip", "tools")
            os.remove("tools\\mycurl.zip")
            print("\n")
            
    def _checkExe(self) -> None:
        exes = [
            {
                "name": "youtubedl",
                "local": Setting.cwd + "\\tools\\youtubedl.exe",
                #"link": "https://yt-dl.org/downloads/2021.04.26/youtube-dl.exe",
                "link": "https://github.com/yt-dlp/yt-dlp/releases/download/2022.01.21/yt-dlp.exe",
                
            },
            {
                "name": "annie",
                "local": Setting.cwd + "\\tools\\annie.exe",
                #"link": "https://github.com/iawia002/annie/releases/download/0.10.3/annie_0.10.3_Windows_64-bit.zip",
                "link": "https://github.com/iawia002/annie/releases/download/0.9.6/annie_0.9.6_Windows_64-bit.zip",
            },
            {
                "name": "n_m3u8dl",
                "local": Setting.cwd + "\\tools\\myng.exe",
                # win10 doesnt support v2.9.9
                "link": "https://github.com/nilaoda/N_m3u8DL-CLI/releases/download/2.9.7/N_m3u8DL-CLI_v2.9.7.exe",
            },
            {
                "name": "curl",
                "local": Setting.cwd + "\\tools\\mycurl.exe",
                "link": "https://files.catbox.moe/ze4uva.zip",
            }
        ]

        for exeInfo in exes:
            if not os.path.isfile(exeInfo["local"]):
                self._getExe(exeInfo)

setting = Setting()
toolPath = os.getcwd() + "\\tools"
os.environ["path"] += f";{toolPath}"