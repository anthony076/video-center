// Routes class is created by you.
import 'package:flutter/material.dart';
import 'package:sailor/sailor.dart';

import 'package:flutter_app/main/mainPage.dart';
import 'package:flutter_app/check/CheckPage.dart';
import 'package:flutter_app/video/playerPage.dart';
import 'package:flutter_app/video/listPage.dart';
import 'package:flutter_app/setting/settingPage.dart';

class Routes {
  static final sailor = Sailor();

  static void createRoutes() {
    sailor.addRoutes([
      // ===============
      SailorRoute(
        name: "/check",
        builder: (context, args, params) {
          return CheckPage();
        },
      ),
      // ===============
      SailorRoute(
        name: "/main",
        builder: (context, args, params) {
          return MainPage();
        },
      ),
      // ===============
      SailorRoute(
          name: "/list",
          builder: (context, args, params) {
            return ListPage();
          }),
      // ===============
      SailorRoute(
        name: "/player",
        builder: (context, args, params) {
          final url = params.param<String>('url');
          return PlayerPage(url: url, key: Key("player"));
        },
        params: [
          SailorParam<String>(
            name: 'url',
          ),
        ],
      ),
      // ===============
      SailorRoute(
        name: "/setting",
        builder: (context, args, params) {
          return SettingPage();
        },
      ),
    ]);
  }
}
