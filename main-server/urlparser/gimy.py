from asyncio import subprocess
import re
import asyncio
from subprocess import run
from os import path, getcwd
from uuid import uuid1

from multidict import MultiDictProxy
from typing import Tuple, Optional

import requests
from ujson import loads
from pyquery import PyQuery
from urllib.parse import urlparse, quote

from urlparser.abc import ParserBase, GimyBase
from utils.logger import loggers

gimyLogger = loggers["gimy"].createLogger()

def getHost(thirdUrl: str) -> Optional[str]:
    host = None

    gimyLogger.info(f"thirdUrl = {thirdUrl}")

    if "www.iqiyi.com" in thirdUrl:
        host =  "iqiyi"
    elif "v.qq.com" in thirdUrl:
        host =  "qq"
    elif "v.youku.com" in thirdUrl:
        host =  "youku"
    elif "vod.bunediy.com" in thirdUrl:
        host =  "bunediy"
    elif "renrenmi" in thirdUrl:
        host =  "renrenmi"
    else:
        gimyLogger.info("cant find host in thirdUrl")
        
    gimyLogger.info(f"host = {host}")

    return host

# ==============
# for 電視類影片，https://gimy.app/
# ==============
class _Gapp(GimyBase):
    '''
        流程
        parse()
            -> step1，_epPageUrls(), get all ep pages 
            -> step2，_fetch(), get content from _epPageUrl, and store as epPage
            -> step3，get vid、videoname、m3u8url from epPage
            -> step4，_m3u8Url(), get video url from epPage
            -> step5，create download-information to json
    '''
    def __init__(self, collectPageUrl: str):
        self.collectPageUrl = collectPageUrl
        self._sess = requests.session()

    # ========================= 
    # 發起請求，取回網頁內容
    # =========================
    def _fetch(self, url: str) -> str:
        content = ""

        try:
            resp = self._sess.get(url, timeout=20)

            if resp.status_code != 200:
                gimyLogger.info(f"status_code:{resp.status_code}, can't fetch content")
            else:
                content = resp.text

        except requests.exceptions.ReadTimeout:
            gimyLogger.info(f"timeout @ {url}")

        except Exception as e:
            gimyLogger.info(f"{e}")

        return content

    # ========================= 
    # 從 collectPageUrl 的網址取得 pid
    # =========================
    def _pid(self, collectPageUrl: str) -> int:
        """
        page-id, https://gimy.app/vod/<pid>.html
        """
        try:
            pid = collectPageUrl.replace("https://", "").replace(".html", "").split("/")[2]
        except Exception:
            pid = None
        
        gimyLogger.debug(f"pid={pid}")
        return pid

    # ========================= 
    # 從 epPageUrl 的內容中取得 videoName
    # =========================
    def _videoName(self, epPageUrl: str, epPageContent: str) -> Tuple[str, str]:
        vid = None
        videoName = None

        re_title = r'<title>.*<\/title>'
        findTitle = re.search(re_title, epPageContent)

        if findTitle:
            # https://gimy.app/eps/576-1-2.html
            vid = epPageUrl.replace("https://gimy.app/eps/", "").replace(".html", "")   # 576-1-2
            
            [_, _, epid] = vid.split("-")  # 2
            videoName = PyQuery(findTitle.group()).text().split(" ")[0] + f"-{epid}"

        gimyLogger.debug(f"vid={vid}, videoName={videoName}")
        return vid, videoName

    # ========================= 
    # 從第一次的 indexM3u8Url 的內容，取得真實的 m3u8 網址
    # =========================
    def _getRealM3u8(self, indexM3u8Url: str) -> Optional[str]:
        """
        從 indexM3u8Url 的 response 取得真實m3u8 的網址
        """
        parsedUrl = urlparse(indexM3u8Url)
        hostname = parsedUrl.hostname
        scheme = parsedUrl.scheme
        
        re_m3u8Url = r'\/.*\.m3u8'

        raw = self._fetch(indexM3u8Url)

        find = re.search(re_m3u8Url, raw)
        if find:
            return scheme + "://" + hostname + find.group()
        else:
            return None

    # ========================= 
    # 從 epPage 中取得第一次的 m3u8 網址 (indexM3u8Url)
    # =========================
    def _m3u8Url(self, playerUrl: str):
        m3u8Url = None

        content = self._fetch(playerUrl)

        if not content:
            # 取回內容為空，無法獲取 m3u8Url
            m3u8Url = None
        elif len(content) < 2000:
            # 當前 m3u8 是 index-m3u8，需要進行二次取回
            m3u8Url = self._getRealM3u8(playerUrl)
        else:
            # 當前 m3u8 已經是 real-m3u8，不需要進行二次取回
            m3u8Url = playerUrl

        gimyLogger.debug(f"m3u8Url={m3u8Url}")

        return m3u8Url

    # ========================= 
    # 從 epPage 中取得非 m3u8 的影片網址
    # 例如，epPageUrl = https://www.iqiyi.com/v_19rrarpa7s.html"
    # =========================
    def _nonM3u8Url(self, thirdUrl: str):
        """
        非 m3u8 的網址需要透過 getRealUrl.exe 還原出外連網頁的原始網址
        """
        videoUrl = None

        # get reponse of thirdUrl via https://gimy.app/jcplayer/?url=<thirdUrl>
        reqUrl = f"https://gimy.app/jcplayer/?url={thirdUrl}"
        content = self._fetch(reqUrl)

        re_config = r'getVideoInfo\(".+"\)'
        findConfig = re.search(re_config, content)

        if findConfig != None:
            
            # findConfig = getVideoInfo("jy0dn6sb ... gnz")
            # encryptCode = jy0dn6sb ... gnz
            encryptCode = findConfig.group()[14:-2]
            gimyLogger.info(f"encryptCode = {encryptCode}")

            command = [
                path.join(getcwd(), "tools", "decode.exe"), 
                # thirdUrl =  "https://v.qq.com/x/cover/ehjvbzck9pcxhkp/y0022qf5odc.html
                "-h", getHost(thirdUrl), 
                "-t", encryptCode,
            ]

            # get decoded-url from decode.exe
            videoUrl = run(command, shell=False, capture_output=True, encoding="utf-8").stdout.strip()
            gimyLogger.debug(f"videoUrl = {videoUrl}")

        return videoUrl

    # ========================= 
    # 解析 epPageUrl，判斷影片類型
    # =========================
    def parse_url(self, epPageUrl: str, epPageContent: str):

        re_videourl = r'player_data=\{.*\}'

        PlayerData = re.search(re_videourl, epPageContent)
        playerUrl: dict = loads(PlayerData.group().split("=")[1])["url"]

        if "m3u8" in playerUrl:
            return self._m3u8Url(playerUrl)
        else:
            return self._nonM3u8Url(playerUrl)

    # ========================= 
    # 解析 collectPageUrl 的網頁內容，取得所有分集的網址
    # =========================
    def _epPageUrls(self, collectPageUrl: str) -> list:
        tmp = {}

        hostname = urlparse(collectPageUrl).hostname
        scheme = urlparse(collectPageUrl).scheme
        pid = self._pid(collectPageUrl)

        raw = self._fetch(collectPageUrl)
        allATags = PyQuery(raw)("a")

        # ==== 篩選 所有的 aTag ====
        for el in allATags.items():
            # /eps/144141-1-1.html
            href = el.attr("href")

            try:
                # 只獲取與當前影片相關的分集Url : 只取得有 href 屬性的 atag，且 href 包含 vid 變數的連結
                if (href is not None) & (pid in href):
                    _, server, _ = href.replace("/eps/", "").replace(".html", "").split("-")

                    # 依照 server name 儲存 url，並透過 set() 進行去重
                    if server not in tmp.keys():
                        tmp[server] = set()

                    tmp[server].add(scheme + "://" + hostname + href)
            except Exception as e:
                gimyLogger.info(f"{e}")
                continue
        
        # ==== 從多個片源中，篩選最完整的片源 ====
        links = []
        maxLen = 0
        for server in tmp.keys():
            curLen = len(tmp[server])
            if curLen > maxLen:
                links = list(tmp[server])
                maxLen = curLen

        if len(links) < 1:
            gimyLogger.info("cant find any epPageUrl")
        else:
            gimyLogger.debug(f"epPages={links}")
        
        return links

    # ========================= 
    # 對 collectPageUrl 進行解析
    # =========================
    def parse(self) -> list:
        gimyLogger.info(f"[gimy][_Gapp] processing {self.collectPageUrl}")

        Infos: list[dict] = []

        # ==== get epPages ====
        if "/eps/" in self.collectPageUrl:
            epPages = [self.collectPageUrl]
        else:
            epPages = self._epPageUrls(self.collectPageUrl)

        # ==== get m3u8Url、vid、videoName ====
        for epPage in epPages:
            gimyLogger.info("===============================")

            epPageContent: str = self._fetch(epPage)

            #m3u8Url = self._m3u8Url(epPage, epPageContent)
            m3u8Url = self.parse_url(epPage, epPageContent)

            vid, videoName = self._videoName(epPage, epPageContent)

            if all([m3u8Url != None, vid != None, videoName != None]):
                Infos.append({"m3u8Url": m3u8Url, "vid": vid, "videoName": videoName})

        return Infos
        
# ==============
# for 電影類影片，https://gimytv.com/
# ==============
class _Gtv(GimyBase):
    def __init__(self, collectPageUrl: str):
        self.collectPageUrl = collectPageUrl
        self._sess = requests.session()
    
    def _fetch(self, url: str) -> str:
        content = ""

        try:
            resp = self._sess.get(url, timeout=20)

            if resp.status_code != 200:
                gimyLogger.info(f"status_code:{resp.status_code}, can't fetch content")
            else:
                content = resp.text

        except requests.exceptions.ReadTimeout:
            gimyLogger.info(f"timeout @ {url}")

        except Exception as e:
            gimyLogger.info(f"{e}")

        return content

    def _pid(self, collectPageUrl: str) -> int:
        """
        page-id, https://gimytv.com/v_<pid>.html
        """

        try:
            pid = collectPageUrl.replace("https://", "").replace(".html", "").split("/")[1].split("_")[1]
        except Exception:
            pid = None
        
        gimyLogger.debug(f"pid={pid}")
        return pid

    def _getRealM3u8(self, indexM3u8Url: str) -> Optional[str]:
        """
        從 indexM3u8Url 的 response 取得真實m3u8 的網址
        """
        parsedUrl = urlparse(indexM3u8Url)
        hostname = parsedUrl.hostname
        scheme = parsedUrl.scheme
        
        re_m3u8Url = r'\/.*\.m3u8'

        raw = self._fetch(indexM3u8Url)

        find = re.search(re_m3u8Url, raw)
        if find:
            return scheme + "://" + hostname + find.group()
        else:
            return None

    def _m3u8Url(self, epPageUrl: str, epPageContent: str):
        """
        注意，有的片源的 m3u8Url 為空，會出現 m3u8Url = None
        """
        m3u8Url = None

        re_indexM3u8Url = r'player_data=\{.*\}'
        findPlayerData = re.search(re_indexM3u8Url, epPageContent)

        if findPlayerData:
            playerData: dict = loads(findPlayerData.group().split("=")[1])
            gimyLogger.debug(f"playerData={playerData}")

            # ==== 判斷當前 m3u8Url 是 index-m3u8 或 real-m3u8 ====   
            content = self._fetch(playerData["url"])

            if not content:
                # 取回內容為空，無法獲取 m3u8Url
                m3u8Url = None
            elif len(content) < 2000:
                # 當前 m3u8 是 index-m3u8，需要進行二次取回
                m3u8Url = self._getRealM3u8(playerData["url"])
            else:
                # 當前 m3u8 已經是 real-m3u8，不需要進行二次取回
                m3u8Url = playerData["url"]

        gimyLogger.debug(f"m3u8Url={m3u8Url}")
        return m3u8Url

    def _videoName(self, epPageUrl: str, epPageContent: str) -> Tuple[str, str]:
        vid = None
        videoName = None

        re_title = r'<title>.*<\/title>'
        findTitle = re.search(re_title, epPageContent)

        if findTitle:
            # https://gimytv.com/ep-eFN-2-1.html
            vid = epPageUrl.replace("https://gimytv.com/ep-", "").replace(".html", "")   # eFN-2-1
            [_, _, epid] = vid.split("-")  # 1
            videoName = PyQuery(findTitle.group()).text().split(" ")[0] + f"-{epid}"

        gimyLogger.debug(f"vid={vid}, videoName={videoName}")
        return vid, videoName

    def _epPageUrls(self, collectPageUrl: str) -> list:
        tmp = {}

        hostname = urlparse(collectPageUrl).hostname
        scheme = urlparse(collectPageUrl).scheme
        pid = self._pid(collectPageUrl)

        raw = self._fetch(collectPageUrl)
        allATags = PyQuery(raw)("a")

        # ==== 篩選 所有的 aTag ====
        for el in allATags.items():
            # /ep-h36-6-1.html
            href = el.attr("href")

            try:
                # 只獲取與當前影片相關的分集Url : 只取得有 href 屬性的 atag，且 href 包含 vid 變數的連結
                if (href is not None) & (pid in href):
                    _, server, _ = href.replace("ep-", "").replace(".html", "").split("-")

                    # 依照 server name 儲存 url，並透過 set() 進行去重
                    if server not in tmp.keys():
                        tmp[server] = set()

                    tmp[server].add(scheme + "://" + hostname + href)
            except Exception as e:
                gimyLogger.info(f"{e}")
                continue
        
        # ==== 從多個片源中，篩選最完整的片源 ====
        links = []
        maxLen = 0
        for server in tmp.keys():
            curLen = len(tmp[server])
            if curLen > maxLen:
                links = list(tmp[server])
                maxLen = curLen

        if len(links) < 1:
            gimyLogger.info("cant find any epPageUrl")
        else:
            gimyLogger.debug(f"epPages={links}")
        
        return links

    def parse(self) -> list:
        gimyLogger.info(f"[gimy][_Gtv] processing {self.collectPageUrl}")

        Infos: list[dict] = []

        # ==== get epPages ====
        if "ep-" in self.collectPageUrl:
            # https://gimytv.com/ep-36N-1-1.html
            epPages = [self.collectPageUrl]
        else:
            # https://gimytv.com/v_Nge.html
            epPages = self._epPageUrls(self.collectPageUrl)

        # ==== get m3u8Url、vid、videoName ====
        for epPage in epPages:
            epPageContent: str = self._fetch(epPage)

            m3u8Url = self._m3u8Url(epPage, epPageContent)
            vid, videoName = self._videoName(epPage, epPageContent)

            if all([m3u8Url != None, vid != None, videoName != None]):
                Infos.append({"m3u8Url": m3u8Url, "vid": vid, "videoName": videoName})

        return Infos

# ==============
# Gimy 的基類
# ==============
class Gimy(ParserBase):
    def __init__(self):
        pass

    def _write2Bat(self, downloadInfos: list) -> Optional[str]:
        # 序列下載的 batch 檔，
        # 注意，batch 檔支援中文差，需要中文的地方改用 mycurl 進行處理
        batchFileName = f"m3u8-{uuid1()}.bat"
        validCmdCount: int = 0

        with open(batchFileName, "wb") as fw:

            # 尋找每個分頁的 m3u8Url、videoName、vid
            for infoMap in downloadInfos:
                m3u8Url = infoMap["m3u8Url"]
                videoName = infoMap["videoName"]
                vid = infoMap["vid"]

                # 必要參數為空，跳過當前下載任務
                if any([m3u8Url is None, videoName is None, vid is None]):
                    continue

                elif ".m3u8" in m3u8Url:
                    # 建立下載命令
                    cmd = "myng" + f" \"{m3u8Url}\" --saveName \"{vid}\" --enableDelAfterDone --enableBinaryMerge \n"
                    fw.write(cmd.encode("utf-8"))

                    # 複製到 video 資料夾中
                    fw.write(f"copy /Y Downloads\{vid}.ts videos \n".encode("utf-8"))
                    
                    # 刪除 Download 資料夾中已下載的檔案
                    fw.write(f"del /F /Q Downloads\{vid}.ts \n".encode("utf-8"))
                    
                    # 變更為中文名的處理，透過 curl 進行檔案更名
                    # 注意，不要使用 \ 的路徑，改用 / ，避免路徑錯誤
                    oldName = quote(f"videos/{vid}.ts")
                    newName = quote(f"videos/{videoName}.ts").replace("%", "@")
                    
                    # 在 batch 進行更名
                    fw.write(f'mycurl "http://127.0.0.1:5566/rename?old={oldName}&new={newName}"\n\n'.encode("utf-8"))
                    validCmdCount += 1

                elif ".mp4" in m3u8Url:
                    # 建立下載命令
                    cmd = "youtubedl" + f" \"{m3u8Url}\" --output \"Downloads\{vid}.mp4\" \n"
                    fw.write(cmd.encode("utf-8"))

                    # 複製到 video 資料夾中
                    fw.write(f"copy /Y Downloads\{vid}.mp4 videos \n".encode("utf-8"))
                    
                    # 刪除 Download 資料夾中已下載的檔案
                    fw.write(f"del /F /Q Downloads\{vid}.mp4 \n".encode("utf-8"))
                    
                    # 變更為中文名的處理，透過 curl 進行檔案更名
                    # 注意，不要使用 \ 的路徑，改用 / ，避免路徑錯誤
                    oldName = quote(f"videos/{vid}.mp4")
                    newName = quote(f"videos/{videoName}.mp4").replace("%", "@")
                    
                    # 在 batch 進行更名
                    fw.write(f'mycurl "http://127.0.0.1:5566/rename?old={oldName}&new={newName}"\n\n'.encode("utf-8"))
                    validCmdCount += 1
                else:
                    gimyLogger.info("Unsupported media type")

            # 在 batch 中更新資料庫
            if validCmdCount > 0:
                # validCmdCount > 0 代表有下載命令，有下載命令才需要更新資料庫
                fw.write("mycurl http://127.0.0.1:5566/scan\n".encode("utf-8"))
                fw.write("exit\n".encode("utf-8"))
                
                gimyLogger.debug(f"batchFileName={batchFileName}")
                return batchFileName
            else:
                return None

    def _genArgs(self, params: dict):
        """ transform url parameter into command args """
        pass

    async def _download(self, batchFileName) -> Tuple[int, str, str]:
        try:
            await asyncio.create_subprocess_shell(f"start {batchFileName}")
            return 0, "", ""
        except Exception as e:
            return 98, "", f"[subprocess-err] {e}"

    async def download(self, params: MultiDictProxy) -> Tuple[int, str, str]:
        """
        download()，由 Task.run() 調用
        """
        url: str = params["u"]
        
        if "gimy.app" in url:
            g = _Gapp(url)
        elif "gimytv" in url:
            g = _Gtv(url)
        else:
            gimyLogger.info("[gimy] Unknown url")
            return 99, "", "Unknown url"

        downloadInfos = g.parse()

        if len(downloadInfos) > 0:
            batchFileName = self._write2Bat(downloadInfos)
            await self._download(batchFileName)
            return 80, "", ""
        else:
            return 90, "", ""