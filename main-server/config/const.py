
from dataclasses import dataclass

@dataclass
class Const:
    EMPTY = ""
    STATUS_SUCCESS = "Success"
    STATUS_FAIL = "Fail"

    MSG_MISS_PARA = "Missing parameter"
    MSG_FILE_SUCCESS = "File operation success"
    MSG_FILE_MISSING = "File not exist"
    MSG_FILE_DEL_ERR = "File delete error"
    MSG_FILE_MOV_ERR = "File move error"
    MSG_FILE_REN_ERR = "Rename file error"

    MSG_FD_SUCCESS = "Folder operation success"
    MSG_FD_CREATE_ERR = "Folder create error"

    MSG_DB_DEL_ERR = "DB delete record error"
    MSG_DB_UPDATE_ERR = "DB update record error"

    @staticmethod
    def JSON_FD_LIST() -> dict:
        result = {
            "status": None,
            "folders": [],
            "msg": Const.EMPTY,
        }

        return result

    @staticmethod
    def JSON_FILE_DELETE_ALL() -> dict:
        result = {
            "status": None,
            "deleted": [],
            "fail": [],
        }

        return result

    JSON_FILE_SUCCESS = {
        "status": STATUS_SUCCESS,
        "msg": MSG_FILE_SUCCESS,
    }

    JSON_FILE_MISS = {
        "status": STATUS_FAIL,
        "msg": MSG_FILE_MISSING,
    }

    JSON_FILE_DEL_ERR = {
        "status": STATUS_FAIL,
        "msg": MSG_FILE_DEL_ERR,
    }

    JSON_FILE_MOV_ERR = {
        "status": STATUS_FAIL,
        "msg": MSG_FILE_MOV_ERR,
    }

    JSON_DB_UPDATE_ERR = {
        "status": STATUS_FAIL,
        "msg": MSG_DB_UPDATE_ERR,
    }

    JSON_FD_SUCCESS = {
        "status": STATUS_SUCCESS,
        "msg": MSG_FD_SUCCESS,
    }

    JSON_FD_CREATE_ERR = {
        "status": STATUS_FAIL,
        "msg": MSG_FD_CREATE_ERR,
    }

    JSON_SCAN_DONE = {
        "status": STATUS_SUCCESS,
        "msg": EMPTY,
    }

    JSON_RENAME_DONE = {
        "status": STATUS_SUCCESS,
        "msg": EMPTY,
    }

    JSON_RENAME_Err = {
        "status": STATUS_FAIL,
        "msg": MSG_FILE_REN_ERR,
    }

    JSON_MISS_Parameter = {
        "status": STATUS_FAIL,
        "msg": MSG_MISS_PARA,
    }

    JSON_Download_ADDTASK = {
        "status": STATUS_SUCCESS,
        "tid": 0,
    }
