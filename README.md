## 使用說明
- step1，將影片放在 server 專案內的 videos 資料夾中
    預設 server 啟動後，會掃描該資料夾，並將影片資訊添加到 video.db 檔案中

- step2，啟動 server
    在 server 的專案目錄中，輸入 python server.py

- step3 (選項1)，使用 android-simulator 進行測試
    3-1，確保代碼中的連結使用的是 10.0.2.2，而不是 127.0.0.1 或 192.168.x.x 
    3-2，打開 app 後進行測試

- step3 (選項2)，使用 android 手機 進行測試
    3-1，確保 android 手機 和 server 連接到同一個 wifi
    3-2，確保代碼中的連結使用的是 127.0.0.1 或 192.168.x.x，而不是 10.0.2.2
    3-3，打開 app 後進行測試