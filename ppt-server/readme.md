## 參考
- running puppeteer on docker
    https://github.com/puppeteer/puppeteer/blob/main/docs/troubleshooting.md#running-puppeteer-in-docker

## 注意事項
- 使用 typescript 開發時，應該使用相對路徑
    不要使用絕對路徑，例如，src/lib/aa，tsc 編譯後的實際路徑為 build/lib/aa，
    開發的路徑和編譯後的路徑是不一樣的

## 使用
- step0，設定 chrome 位置
    > set_chrome_env.bat
    
- step1，安裝全局套件
    > run_install_global.bat

- step2，安裝專案套件
    > npm install

- step3，啟動 typescript
    - 單次編譯
    > tsc
    - 編譯專案並啟動監聽模式
    > tsc --watch

- step4，本機開發
    > npm run start

- step5，開啟 docker-engine
    - For docker-toolbox
    > run_docker_toolbox.bat

    - For docker Desktop
    > "C:\Program Files\Docker\Docker\Docker Desktop.exe"

- step6，建立 image
    注意，若使用 docker-toolbox，需要手動設定 virtualbox 的 port-forwarding
    > docker build --no-cache -t ppt/image .

- step6，建立容器
    > 手動，docker run --rm -p 5567:5567 --name ppt -it ppt/image /bin/sh
    > 自動，docker run -d --rm -p 5567:5567 --name ppt ppt/image

- step7，測試
    > 在本機瀏覽器執行，http://127.0.0.1:5567/ping

## 開發
- 使用 puppeteer-extra-plugin-stealth + typescript 時，puppeteer 需要降版本為 puppeteer@5，
    最新版的 puppeteer 與 puppeteer-extra-plugin-stealth 的 type-define 不一致

- 運行在 docker 上時，出現  Failed to launch the browser process! 錯誤
    https://yami.io/alpine-puppeteer-enoent/
    https://github.com/puppeteer/puppeteer/issues/3994

    停止 puppeteer 自動下載的功能，手動安裝 chrome，並指定執行位置

- 在 docker 中運行 puppeteer，需要添加 --no-sandbox 和 --disable-setuid-sandbox 的參數
    https://stackoverflow.com/questions/62345581/node-js-puppeteer-on-docker-no-usable-sandbox

- 若檔案類型是 `mp4`，必須要點擊第三個廣告button，才會發出 mp4 的 request

- 若檔案類型是 `m3u8`，在載入 targetFrame 的過程中，就會預先載入 m3u8 的檔案
    因此不需要找到 targetFrame，也不需要點擊第三個廣告 button，
    只需要點擊第二個廣告 button 後開啟 req 監聽

## Bug
- 在 docker 中的 puppeteer 解析大型網頁時，因記憶體不足造成 chrome crash 的問題，
    預設 docker container 僅使用 64MB 記憶體，啟動瀏覽器時，添加 `--disable-dev-shm-usage` 來避免此問題
    https://github.com/puppeteer/puppeteer/blob/main/docs/troubleshooting.md#tips

## Todo
- hpjav 有不同的片源，(ns、ac、st、ds、ss)、md、vo，目前只完成 ns

