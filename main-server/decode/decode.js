var CryptoJS = require("crypto-js");

function getArgv(parameter) {
    try {
        index = process.argv.indexOf(parameter)
    } catch (error) {
        throw Error(parameter + " not provide")
    }

    return process.argv[index + 1]
}

function v_decrypt(data, token_key, token_iv) {
    return CryptoJS.AES.decrypt(data, token_key, { iv: token_iv }).toString(CryptoJS.enc.Utf8);
}

function getVideoInfo(url) {
    return v_decrypt(url, _token_key, _token_iv)
}

var _token_key = CryptoJS.enc.Utf8.parse("A42EAC0C2B408472");//此密钥要跟config.php里的video_key要一致，否则会导致无法解密
var _token_iv = CryptoJS.enc.Utf8.parse("47852a2f5166bf85");

var hostMap = {
    "iqiyi": "https://jiemi3.rx",   // 藍光雲
    "qq": "https://apd-vlive",      // 超清雲
    "mgtv": "https://112.1.130",    // 芒果雲
    "youku": "https://vip.youku",   // 優酷雲
    "bunediy": "https://vod.buned",      // 快播雲
    "renrenmi": "https://cloud.ren",     // 高清雲      
}

url = getArgv("-t")
host = getArgv("-h")

decoded = getVideoInfo(url)
console.log(hostMap[host] + decoded.slice(17, decoded.length))
