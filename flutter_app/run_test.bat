@echo off

cd %~dp0
echo ===== EXECUTE run_clean.bat ON Server side FIRST, and have Simulaor READY ====
pause 
:: ==== setup env ===

:: check env
set env=inside
if not x%cd:Administrator=%==x%cd% set env=outside

:: set folder
set videoSrc=C:\Users\%username%\Desktop\videos
set videoDist=C:\Users\%username%\Desktop\server\videos
set serverBatch=C:\Users\%username%\Desktop\server\run_server.bat

if %env%==outside (
    set videoSrc=C:\Users\Administrator\Desktop\videos
    set videoDist=C:\Users\Administrator\Desktop\server\videos
    set serverBatch=C:\Users\Administrator\Desktop\server\run_server.bat
)

:: ==== For Android ====
@REM 執行 offline 測試
::call flutter drive --driver=test_driver/driver.dart --target=test_widget/offline_test.dart

@REM 複製影片
xcopy /E /I /Y %videoSrc% %videoDist%

@REM 啟動server
start %serverBatch%

@REM 更新資料庫
start curl http://127.0.0.1:5566/scan

@REM 延遲 5 秒
:: delay 5 sec after start server
ping 127.0.0.1 -n 5 -w 1000 > nul

@REM 執行 online 測試
call flutter drive --driver=test_driver/driver.dart --target=test_widget/online_test.dart

@REM 關閉 server
start curl http://127.0.0.1:5566/quit


:: ==== For Web ====
:: 進行測試前，1_啟動 application-server，2_啟動 chorme-driver @ port 4444
:: start chromedriver --port=4444
::flutter drive --driver=test_driver/driver.dart --target=test_widget/foo_test.dart -d web-server
::flutter drive --driver=test_driver/driver.dart --target=test_widget/foo_test.dart -d chrome
