*** Settings ***
Documentation
...        1，使用前安裝所有必要套件 pip install -r requirements.txt
...        2，測試前停止已經啟動的 server
...        3，手動執行測試，run_test.bat
...        run_test.bat 會 1_刪除 db、video-folder，2_複製 video-folder，3_啟動 server

Library    BuiltIn
Library    OperatingSystem
Library    Process
Library    REST                                                                 http://127.0.0.1:5566

*** Variable ***

*** Test Case ***
測試系統狀態
    GET /ping

測試[列表]目錄資料夾
    GET /lf

測試[列表]影片列表
    GET /list

測試[列表]影片列表 - 根據目錄名稱
    GET /list?dst=dd
    # TODO

測試[影片]撥放 - 未提供影片id
    GET /v

測試[影片]撥放 - 提供錯誤id
    GET /v?id=123

測試[目錄]建立 - 未提供目錄名稱
    GET /mf

測試[目錄]建立 - 有提供目錄名稱
    GET /mf?name=dd

測試[影片]移動 - 未提供參數
    GET /mv

測試[影片]移動 - 提供錯誤的參數 - 檔案id錯誤
    GET /mv?ids=ttt&dst=dd

測試[影片]移動 - 提供錯誤的參數 - 目標路徑錯誤
    GET /mv?ids=97430f319fb9638218bc59b36d473060&dst=123

測試[影片]移動 - 正確的使用
    GET /mv?ids=97430f319fb9638218bc59b36d473060&dst=dd

測試[影片]移動 - 移動多個影片
    GET /mv?ids=0a85b6a6241c97c002175ce331eaa20c,ebce0ca352df39ed49fda5467b6025b5&dst=dd

測試[影片]刪除 - 未提供參數
    GET /r

測試[影片]刪除 - 提供錯誤的參數
    GET /r?id=123&all=123

測試[影片]刪除 - 檔案不存在
    GET /r?id=123

測試[影片]刪除 - 刪除單一影片
    GET /r?id=97430f319fb9638218bc59b36d473060

測試[影片]刪除 - 刪除多部影片
    GET /r?all=e72bf6080df799ac824811afb346dac1,d1e0ba4aa8df58e366e2fbdc48df279f,d405fb085cdffb6968f3e6297568821d

*** Keywords ***
GET /ping
    [Documentation]                                                                                                  取得系統狀態
    [Tags]                                                                                                           系統
    GET                                                                                                              /ping
    String                                                                                                           response body                                                                                                pong

GET /lf
    [Documentation]                                                                                                  目錄列表
    [Tags]                                                                                                           列表
    GET                                                                                                              /lf
    Integer                                                                                                          response status                                                                                              200
    String                                                                                                           response body status                                                                                         Success
    Array                                                                                                            response body folders                                                                                        minItems=1
    String                                                                                                           response body folders 0                                                                                      videos

GET /list
    [Documentation]                                                                                                  影片列表
    [Tags]                                                                                                           列表
    GET                                                                                                              /list
    Integer                                                                                                          response status                                                                                              200
    Object                                                                                                           response body                                                                                                required=["videos"]
    Array                                                                                                            response body videos                                                                                         minItems=1

GET /list?dst=dd
    [Documentation]                                                                                                  影片列表 - 根據目錄名稱
    [Tags]                                                                                                           列表
    GET                                                                                                              /list?dst=dd
    Integer                                                                                                          response status                                                                                              200
    Object                                                                                                           response body                                                                                                required=["videos"]
    Array                                                                                                            response body videos                                                                                         maxItems=0

GET /v
    [Documentation]                                                                                                  未提供影片id
    [Tags]                                                                                                           影片
    GET                                                                                                              /v
    Integer                                                                                                          response status                                                                                              200
    String                                                                                                           response body                                                                                                missing parameter

GET /v?id=123
    [Documentation]                                                                                                  提供錯誤id
    [Tags]                                                                                                           影片
    GET                                                                                                              /v?id=123
    Integer                                                                                                          response status                                                                                              200
    Object                                                                                                           response body                                                                                                required=["status", "msg"]
    String                                                                                                           response body status                                                                                         Fail
    String                                                                                                           response body msg                                                                                            File not exist

GET /mf
    [Documentation]                                                                                                  未提供目錄名稱
    [Tags]                                                                                                           目錄
    GET                                                                                                              /mf
    Integer                                                                                                          response status                                                                                              200
    String                                                                                                           response body                                                                                                missing parameter

GET /mf?name=dd
    [Documentation]                                                                                                  在 video 目錄中建立 dd 的子目錄
    [Tags]                                                                                                           目錄
    GET                                                                                                              /mf?name=dd
    Integer                                                                                                          response status                                                                                              200
    Object                                                                                                           response body                                                                                                required=["status", "msg"]
    String                                                                                                           response body status                                                                                         Success
    String                                                                                                           response body msg                                                                                            Folder operation success

    # 目錄已存在
    GET                                                                                                              /mf?name=dd
    String                                                                                                           response body status                                                                                         Success
    String                                                                                                           response body msg                                                                                            Folder operation success

GET /mv
    [Documentation]                                                                                                  未提供參數
    [Tags]                                                                                                           影片
    GET                                                                                                              /mv
    Integer                                                                                                          response status                                                                                              200
    String                                                                                                           response body                                                                                                missing parameter

GET /mv?ids=ttt&dst=dd
    [Documentation]                                                                                                  提供錯誤的參數 - 檔案id錯誤
    [Tags]                                                                                                           影片
    GET                                                                                                              /mv?ids=ttt&dst=dd
    String                                                                                                           response body ttt status                                                                                     Fail
    String                                                                                                           response body ttt msg                                                                                        File not exist

GET /mv?ids=97430f319fb9638218bc59b36d473060&dst=123
    [Documentation]                                                                                                  提供錯誤的參數 - 目標路徑錯誤
    [Tags]                                                                                                           影片
    GET                                                                                                              /mv?ids=97430f319fb9638218bc59b36d473060&dst=123
    Integer                                                                                                          response status                                                                                              200
    Object                                                                                                           response body                                                                                                required=["97430f319fb9638218bc59b36d473060"]
    String                                                                                                           response body 97430f319fb9638218bc59b36d473060 status                                                        Fail
    String                                                                                                           response body 97430f319fb9638218bc59b36d473060 msg                                                           File move error

GET /mv?ids=97430f319fb9638218bc59b36d473060&dst=dd
    [Documentation]                                                                                                  正確的移動影片
    [Tags]                                                                                                           影片
    GET                                                                                                              /mv?ids=97430f319fb9638218bc59b36d473060&dst=dd
    Object                                                                                                           response body                                                                                                required=["97430f319fb9638218bc59b36d473060"]
    String                                                                                                           response body 97430f319fb9638218bc59b36d473060 status                                                        Success
    String                                                                                                           response body 97430f319fb9638218bc59b36d473060 msg                                                           File operation success

GET /mv?ids=0a85b6a6241c97c002175ce331eaa20c,ebce0ca352df39ed49fda5467b6025b5&dst=dd
    [Documentation]                                                                                                  移動多個影片
    [Tags]                                                                                                           影片
    GET                                                                                                              /mv?ids=0a85b6a6241c97c002175ce331eaa20c,ebce0ca352df39ed49fda5467b6025b5&dst=dd
    Object                                                                                                           response body                                                                                                required=["0a85b6a6241c97c002175ce331eaa20c", "ebce0ca352df39ed49fda5467b6025b5"]
    String                                                                                                           response body 0a85b6a6241c97c002175ce331eaa20c status                                                        Success
    String                                                                                                           response body 0a85b6a6241c97c002175ce331eaa20c msg                                                           File operation success
    String                                                                                                           response body ebce0ca352df39ed49fda5467b6025b5 status                                                        Success
    String                                                                                                           response body ebce0ca352df39ed49fda5467b6025b5 msg                                                           File operation success

GET /r
    [Documentation]                                                                                                  未提供參數
    [Tags]                                                                                                           影片
    GET                                                                                                              /r
    Integer                                                                                                          response status                                                                                              200
    String                                                                                                           response body                                                                                                missing parameter

GET /r?id=123&all=123
    [Documentation]                                                                                                  錯誤的提供參數，id和all不能同時提供
    [Tags]                                                                                                           影片
    GET                                                                                                              /r?id=123&all=123
    Integer                                                                                                          response status                                                                                              200
    String                                                                                                           response body                                                                                                error parameter

GET /r?id=123
    [Documentation]                                                                                                  影片不存在
    [Tags]                                                                                                           影片
    GET                                                                                                              /r?id=123
    Integer                                                                                                          response status                                                                                              200
    Object                                                                                                           response body                                                                                                required=["status", "msg"]
    String                                                                                                           response body status                                                                                         Fail
    String                                                                                                           response body msg                                                                                            File not exist

GET /r?id=97430f319fb9638218bc59b36d473060
    [Documentation]                                                                                                  刪除單一影片
    [Tags]                                                                                                           影片
    GET                                                                                                              /r?id=97430f319fb9638218bc59b36d473060
    Integer                                                                                                          response status                                                                                              200
    Object                                                                                                           response body                                                                                                required=["status", "msg"]
    String                                                                                                           response body status                                                                                         Success
    String                                                                                                           response body msg                                                                                            File operation success

GET /r?all=e72bf6080df799ac824811afb346dac1,d1e0ba4aa8df58e366e2fbdc48df279f,d405fb085cdffb6968f3e6297568821d
    [Documentation]                                                                                                  刪除多個影片
    [Tags]                                                                                                           影片
    GET                                                                                                              /r?all=e72bf6080df799ac824811afb346dac1,d1e0ba4aa8df58e366e2fbdc48df279f,d405fb085cdffb6968f3e6297568821d
    Integer                                                                                                          response status                                                                                              200
    Object                                                                                                           response body                                                                                                required=["status", "deleted", "fail"]
    String                                                                                                           response body status                                                                                         Success
    Array                                                                                                            response body deleted                                                                                        minItems=3
    Array                                                                                                            response body fail                                                                                           maxItems=1
