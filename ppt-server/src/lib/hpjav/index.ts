
import puppeteer from 'puppeteer-extra'
import { Browser, Page } from 'puppeteer'

import StealthPlugin from "puppeteer-extra-plugin-stealth";
puppeteer.use(StealthPlugin())

import { decrypt } from "./decryptor";

// =======================
// variables
// =======================

let browser:Browser = null
let page:Page = null

let opts = {
    executablePath: process.env.PUPPETEER_EXECUTABLE_PATH,
    args: [
        // 移除 webdrive 痕跡
        "--disable-blink-features=AutomationControlled",
        // 關閉 iframe 限制
        '--disable-web-security',
        '--disable-features=IsolateOrigins,site-per-process',
        // 在 docker 中運行需要添加
        '--no-sandbox',
        // 在 docker 中運行需要添加
        '--disable-setuid-sandbox',
        // 在 docker 中避免使用 share memory, 增加 puppet 需要的記憶體
        '--disable-dev-shm-usage',
    ],

    devtools: false,
    headless: true,
}

// =======================
// Main
// =======================

async function hpjav(url:string) {
    browser = await puppeteer.launch(opts)

    page = await browser.newPage();
    page.setViewport({ width: 1280, height: 600 })

    await page.goto(url);
    await page.waitForSelector("#preroll > div > div > div > div.pop-skip > button")
    
    // 等待頁面加載
    await page.waitForTimeout(1000)

    //@ts-ignore
    const vsers:Array<string> = await page.evaluate(() => { return window.vser })
    let m3u8:string = ""
    
    try {
        if (vsers){
            let iframeUrl:string = decrypt(vsers)
            await page.goto(iframeUrl, {waitUntil: "load"})
            //await page.waitForNavigation()
        
            //@ts-ignore
            m3u8 = await page.evaluate(() => { return window.jwplayer().hls.url })
            
        } else {
            throw Error("vsers is empty")
        }

    } catch (e){
        console.log(e)
    }
    
    await browser.close();
    
    console.log(`[video] ${m3u8}`)
    return m3u8
}

export { hpjav }