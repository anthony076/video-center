import 'package:flutter_test/flutter_test.dart';
import 'package:integration_test/integration_test.dart';

import 'package:flutter_app/main.dart' as app;
import 'targetWidgets.dart';

void main() {
  IntegrationTestWidgetsFlutterBinding.ensureInitialized();

  // ==== 檢驗 root-widget ====
  Future<Null> _mainWidgetTest(WidgetTester tester) async {
    await tester.pumpWidget(app.MyApp());
    expect(appFinder, findsOneWidget);

    // ==== 檢驗底部導航列 ====
    await tester.pumpAndSettle();
    expect(bottomNaviFinder, findsOneWidget);

    // ==== 測試目錄頁面 ====
    // 點擊目錄頁面
    await tester.tap(selectIcon);
    await tester.pumpAndSettle();
    expect(videoFolder, findsOneWidget);

    // 點擊 videos 目錄
    await tester.tap(videoFolder);
    await tester.pumpAndSettle();
    expect(listFinder, findsOneWidget);

    // // ==== 測試下載頁面 ====
    // await tester.tap(downloadIcon);
    // await tester.pump();
    // expect(downloadContent, findsOneWidget);
  }

  // ==== 測試影片列表頁面 ====
  Future<Null> _listWidgetTest(WidgetTester tester) async {
    await tester.tap(listIcon);
    await tester.pumpAndSettle();
    expect(listFinder, findsOneWidget);
    expect(listItemA, findsOneWidget);
  }

  // ==== 測試影片刪除功能 ====
  Future<Null> _deleteVideoTest(WidgetTester tester) async {
    // 執行長按後，應該出現刪除按鈕
    await tester.longPress(listItemA);
    await tester.pumpAndSettle();
    expect(delBtn, findsOneWidget);

    // 點擊刪除按鈕，item 和 刪除按鈕 都會消失
    await tester.tap(delBtn);
    await tester.pumpAndSettle();
    expect(listItemA, findsNothing);
    expect(delBtn, findsNothing);

    // TODOs
    // 點擊取消鈕，離開選擇模式，項目取消反白，不進行刪除
  }

  // ==== 測試影片撥放功能 ====
  Future<Null> _playVideoTest(WidgetTester tester) async {
    expect(listItemB, findsOneWidget);
    await tester.tap(listItemB);
    await tester.pumpAndSettle(Duration(seconds: 5));
    expect(playerFinder, findsOneWidget);

    // 離開頁面
    await tester.pageBack();
    await tester.pumpAndSettle(Duration(seconds: 5));
    expect(playerFinder, findsNothing);
    expect(listFinder, findsOneWidget);
  }

  // ==== 測試影片移動功能 ====
  Future<Null> _moveVideoTest(WidgetTester tester) async {
    // 長按影片應該出現移動按鈕
    expect(listItemB, findsOneWidget);
    await tester.longPress(listItemB);
    await tester.pumpAndSettle();
    expect(movBtn, findsOneWidget);

    // 點擊移動後，應該出現對話框和新增資料夾的文字框
    await tester.tap(movBtn);
    await tester.pumpAndSettle();
    expect(movDialog, findsOneWidget); // 對話框
    expect(inputField, findsOneWidget); // 輸入框
    expect(dilogEnterBtn, findsOneWidget); // 確定紐

    await tester.enterText(inputField, "aa");
    await tester.pumpAndSettle();

    await tester.tap(dilogEnterBtn);
    await tester.pumpAndSettle(Duration(seconds: 10));
    expect(listItemB, findsNothing);
  }

  // ==== 測試進入點 ====
  // 需要 testWidgets 將 driver 的實例傳進來
  testWidgets("online-server test", (WidgetTester tester) async {
    // 對 sailor-router 進行初始化
    app.main();

    await _mainWidgetTest(tester);
    await _listWidgetTest(tester);
    await _deleteVideoTest(tester);
    await _playVideoTest(tester);
    await _moveVideoTest(tester);
  });
}
