
import asyncio
from multidict import MultiDictProxy
from typing import Tuple
from urlparser.abc import ParserBase
from utils.logger import loggers

y2bLogger = loggers["y2b"].createLogger()

class Youtube(ParserBase):
    def __init__(self):
        self.exe = str(ParserBase.pwd / "tools/youtubedl.exe")
        
    def _genArgs(self, params) -> None:
        y2bLogger.debug(f"incoming params={params}")

        # TODOs，加入對參數的驗證，避免參數不正確而報錯
        isPlayList: bool = params.get("pl", None)
        pis: str = params.get("pis", None)

        if isPlayList == "1":
            self.exe += " --yes-playlist"
        else:
            self.exe += " --no-playlist"

        if pis:
            self.exe += f" --playlist-items {pis}"

        # output & format
        #self.exe += " -f mp4 -o videos\\%(title)s.%(ext)s"
        
        # add output
        self.exe += " -o videos\\%(title)s.%(ext)s"

    async def download(self, params: MultiDictProxy) -> Tuple[int, str, str]:
        url: str = params["u"]

        y2bLogger.info(f"[y2b] processing {url}")

        self._genArgs(params)

        cmd = self.exe + f" \"{url}\""
        y2bLogger.debug(f"cmd={cmd}")

        proc: asyncio.Process = await asyncio.create_subprocess_shell(
            cmd,
            stdout=asyncio.subprocess.PIPE,
            stderr=asyncio.subprocess.PIPE
        )

        stdout, stderr = await proc.communicate()

        return proc.returncode, stdout, stderr
