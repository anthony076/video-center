import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/providers/mainProviders.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'package:flutter_app/providers/commonProviders.dart';
import 'package:flutter_app/providers/itemProvider.dart';

final listStoreProvider = ChangeNotifierProvider((ref) => ListStore(ref));

class ListStore extends ChangeNotifier {
  String reqUrl;
  ProviderReference ref;
  Dio dio;
  String curFolder = "videos";

  List<ChangeNotifierProvider<ItemStore>> listItems;
  List<ItemStore> seletedItems = [];

  AsyncValue<List<String>> folders;
  List<String> latestFolders;

  ListStore(this.ref) {
    dio = ref.read(diolProvider(10000));
    reqUrl = ref.read(urlProvider).reqUrl;

    fetchList();
    fetchFolder();
  }

  // ===========================================
  void updateVideoList(newVideoList){
    listItems = newVideoList;
    notifyListeners();
  }

  // ===========================================
  
  // 判斷當前選擇的 item 是否已經存在在 seletedItems 中
  bool isVideoExist(ItemStore checkItem) {
    for (ItemStore item in seletedItems) {
      if (item.id == checkItem.id) {
        return true;
      }
    }

    return false;
  }

  // 取得 selectItem 的 index
  int getSeleteIndex(ItemStore targetItem) {
    int index = 0;

    for (ItemStore item in seletedItems) {
      if (item.id == targetItem.id) {
        return index;
      }

      index++;
    }

    index = -1;

    return index;
  }

  // 選擇模式下使用，若選擇的 item 已選中，取消該項目的選中狀態
  void addSelected(ItemStore selectItem) {
    if (isVideoExist(selectItem)) {
      int index = getSeleteIndex(selectItem);
      seletedItems.removeAt(index);
    } else {
      seletedItems.add(selectItem);
    }
    notifyListeners();
  }

  // ===========================================
  Future<void> fetchList() async {
    String para;
    Response response;
    List<ChangeNotifierProvider<ItemStore>> result = [];

    // ===== 發送 scan-request =====
    await dio.get(reqUrl + "/scan");

    // ===== 發送 list-request =====
    // form url parameter
    if (curFolder != "videos") para = "?dst=$curFolder";

    // send list-request
    if (para != null) {
      response = await dio.get(reqUrl + "/list" + para);
    } else {
      response = await dio.get(reqUrl + "/list");
    }

    // parse response
    Map respMap = jsonDecode(response.data.toString());

    for (Map row in respMap["videos"]) {
      final provider = ChangeNotifierProvider(
          (ref) => ItemStore(videoName: row["name"], id: "${row["md5"]}")
      );
      
      result.add(provider);
    }

    updateVideoList(result);
    
  }

  Future<List<String>> fetchFolder() async {
    List<String> result = [];

    folders = AsyncValue.loading();
    notifyListeners();

    try {
      Response response = await dio.get(reqUrl + "/lf");
      Map respMap = jsonDecode(response.toString());

      for (String folder in respMap["folders"]) {
        result.add(folder);
      }

      folders = AsyncValue.data(result);
      latestFolders = result;

      notifyListeners();
    } catch (e) {
      folders = AsyncValue.error(e.toString());
    }

    return result;
  }

  Future<void> deletItems() async {
    List<String> ids = [];

    for (ItemStore item in seletedItems) {
      ids.add(item.id);
    }

    String para = ids.join(",");
    String deleteUrl = reqUrl + "/r?all=$para";

    try {
      // 發出刪除請求
      await dio.get(deleteUrl);

      // 重新獲取影片列表
      await fetchList();
    } catch (e) {
      print(e);
    }
  }

  Future<void> moveItems() async {
    List<String> ids = [];
    String dst = ref.read(moveToProvider).state;

    for (ItemStore item in seletedItems) {
      ids.add(item.id);
    }

    String para = ids.join(",");

    // http://127.0.0.1:5566/mv?ids=d405fb085cdffb6968f3e6297568821d,4304c56106f5c046d7ef4fffcff5225a&dst=aa
    String moveUrl = reqUrl + "/mv?ids=$para&dst=$dst";

    try {
      // 發出移動影片的請求
      await dio.get(moveUrl);

      // 重新獲取影片列表
      await fetchList();
    } catch (e) {
      print(e);
    }
  }
}
