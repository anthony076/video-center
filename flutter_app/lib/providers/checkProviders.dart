import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'package:flutter_app/providers/commonProviders.dart';

final checkStoreProvider = ChangeNotifierProvider((ref) => CheckStore(ref));

class CheckStore extends ChangeNotifier {
  ProviderReference ref;
  String reqUrl;
  Dio dio;
  AsyncValue<bool> isServed;

  CheckStore(this.ref) {
    reqUrl = ref.watch(urlProvider).reqUrl;
    dio = ref.read(diolProvider(10000));
    fetchStatus();
  }

  Future<void> fetchStatus() async {
    isServed = AsyncValue.loading();
    notifyListeners();

    try {
      Response response = await dio.get(reqUrl + "/ping");

      if (response.data.toString() == "pong") {
        isServed = AsyncValue.data(true);
        notifyListeners();
      }
    } catch (e) {
      isServed = AsyncValue.error(e);
      notifyListeners();
    }
  }
}
