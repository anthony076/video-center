
## Project
用於擷取視頻網站的實際影片網址，須提供加密的 url 網址後，透過此程式還原

## Usage
- build exe

    `npm run pkg`

- execute

    `decode.exe -h qq -t <encrypted-code> ` for v.qq.com
    `decode.exe -h iqiyi -t <encrypted-code> ` for www.iqiyi.com
    `decode.exe -h mgtv -t <encrypted-code> ` for mongo-tv
    `decode.exe -h youku -t <encrypted-code> ` for vip.youku
    `decode.exe -h bunediy -t <encrypted-code> ` for bunediy