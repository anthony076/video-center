
import re
import asyncio
#import http.client

import ujson
import math
import time
from random import randint

from multidict import MultiDictProxy
from typing import Tuple, Optional
from urlparser.abc import ParserBase
from urllib.request import urlopen, Request

from utils.logger import loggers

biliLogger = loggers["bili"].createLogger()

# youtubedl 不支援 playlist 下載，因此 bilibili 需要改用 annie 下載
class Bili(ParserBase):
    def __init__(self):
        self.exe = str(ParserBase.pwd / "tools/annie.exe")
        self.header = {"user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.129 Safari/537.36"}

    def _fetch(self, url: str) -> Optional[str]:
        try:
            request = Request(url, headers=self.header)
            res = urlopen(request)
        except Exception as e:
            biliLogger.info(f"{e}")
            return None
        return res.read().decode("utf-8")

    def _getManId(self, url) -> Optional[str]:
        biliLogger.debug(f"url={url}")

        try:
            if "space.bilibili.com" in url:
                # space.bilibili.com/505699632?spm_id_from=333.788.b_765f7570696e666f.2
                find = re.search(r"space\.bilibili\.com\/\d+", url)
                mid = find.group().split("/")[1]
                biliLogger.debug(f"mid={mid}")
                
                return mid

            elif "m.bilibili.com":
                # https://m.bilibili.com/space/505699632?from=search
                find = re.search(r"https:\/\/m.bilibili.com\/space\/\d+", url)
                mid = find.group().split("/space/")[1]
                biliLogger.debug(f"mid={mid}")

                return mid

        except Exception:
            biliLogger.info("can't find mid")
            return None
            
    def _getVideoCount(self, mid: str) -> int:
        videoCount: int = 0

        raw = self._fetch(f"https://api.bilibili.com/x/space/arc/search?mid={mid}&pn=1&ps=30&index=1&jsonp=jsonp")
        j = ujson.loads(raw)

        videoCount = int(j["data"]["page"]["count"])

        biliLogger.debug(f"videoCount={videoCount}")
        
        return videoCount

    def _get(self, mid: str, pn: int, ps: int) -> list[str]:
        biliLogger.debug(f"pn={pn}, ps={ps}")

        pageBVs = []
        baseUrl = "https://www.bilibili.com/video/"

        raw = self._fetch(f"https://api.bilibili.com/x/space/arc/search?mid={mid}&pn={pn}&ps={ps}&index=1&jsonp=jsonp")
        biliLogger.debug(f"raw={raw}")
        
        j = ujson.loads(raw)

        for item in j["data"]["list"]["vlist"]:
            pageBVs.append(baseUrl + item["bvid"])

        biliLogger.debug(f"amount of pageBVs={len(pageBVs)}")

        # 若取回內容為空，進行 retry
        if len(pageBVs) == 0:
            delay = randint(5, 10)

            biliLogger.debug(f"retry after {delay}s")
            time.sleep(delay)
            
            return self._get(mid, pn, ps)

        else:
            return pageBVs
        
    def _getAll(self, mid: str, count: int) -> list[str]:
        biliLogger.debug(f"mid={mid}, count={count}")

        BVs = []
        ps = 30
        
        step = math.ceil(count / ps)
        for i in range(step):
            pn = i + 1
            BVs.extend(self._get(mid, pn, ps))

        biliLogger.debug(f"BVs={BVs}")
        return BVs
            
    def _genArgs(self, params) -> None:
        # TODOs，加入對參數的驗證，避免參數不正確而報錯
        biliLogger.debug(f"params={params}")

        isPlayList: bool = params.get("pl", None)
        pis: str = params.get("pis", None)

        if isPlayList == "1":
            self.exe += " -p"

        if pis:
            self.exe += f" -items {pis}"

        # output & format
        self.exe += " -o videos"
        biliLogger.debug(f"exe={self.exe}")

    async def _download(self, cmd) -> Tuple[int, str, str]:
        biliLogger.info(f"[bili] cmd={cmd}")

        proc: asyncio.Process = await asyncio.create_subprocess_shell(
            cmd,
            stdout=asyncio.subprocess.PIPE,
            stderr=asyncio.subprocess.PIPE
        )

        stdout, stderr = await proc.communicate()

        return proc.returncode, stdout, stderr

    async def download(self, params: MultiDictProxy) -> Tuple[int, str, str]:
        url = params["u"]

        biliLogger.info(f"[bili] processing {url}")

        # 手機端傳遞進來的網址，https://m.bilibili.com/space/505699632?from=search
        # 電腦端傳遞進來的網址，https://space.bilibili.com/505699632?spm_id_from=333.788.b_765f7570696e666f.2
        if "space" in url:
            # ==== 下載 space 頁面中的影片 ====
            mid = self._getManId(url)
            count = self._getVideoCount(mid)
            links = self._getAll(mid=mid, count=count)
            
            # 移除 params 中的 -p 參數 + 產生 Args
            # space 頁面中的影片不是透過 playlist 的方式下載，因此不需要 -p 參數
            del params["pl"]
            self._genArgs(params)

            # TODOS，對 bili-space-url 加入篩選影片功能，可選擇要下載的集數

            for link in links:
                cmd = self.exe + f" \"{link}\""
                await self._download(cmd)

            return 0, "", ""
        else:
            # ==== 下載影片頁面或列表頁面的影片 ====
            self._genArgs(params)

            cmd = self.exe + f" \"{url}\""

            result = await self._download(cmd)
            return result
