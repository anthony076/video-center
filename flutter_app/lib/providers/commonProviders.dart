import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:shared_preferences/shared_preferences.dart';

// ==== 提供 sharedPreferences 實例 ====
// 注意，因為 sharedPreferences 實例 必須異步取得，無法直接在 provider 中取得
// 因此此處先不實現 sharedPreferences 實例，而是在外部取得sharedPreferences 實例後，
// 再透過 ProviderScope() 的 override 參數 (@main.dart)注入 
final sharedPreferencesProvider = Provider<SharedPreferences>((ref) {
  throw UnimplementedError();
});

// ==== 提供全局 dio 實例 ====
Dio dio = Dio();
final diolProvider = Provider.autoDispose.family<Dio, int>((ref, timeout) {
  dio.options.connectTimeout = timeout;
  return dio;
});

// ==== 提供是否是 web application ====
Provider<bool> isWebProvider = Provider(
    (_) => defaultTargetPlatform != TargetPlatform.android ? true : false);

// ==== 提供 url ====
ChangeNotifierProvider<UrlStore> urlProvider = ChangeNotifierProvider((_) => UrlStore());
class UrlStore extends ChangeNotifier {
  String _reqUrl;

  UrlStore() {
    init();
  }

  String get reqUrl => _reqUrl;

  void init() async {
    // For web，use http://127.0.0.1:5566
    // For emulator, use http://10.0.2.2:5566
    SharedPreferences pref = await SharedPreferences.getInstance();
    _reqUrl = pref.getString("serverip") ?? "http://127.0.0.1:5566";

    // 初始化也需要使用notifyListeners() 通知讀取初始值的引用們進行更新
    notifyListeners();
  }

  void setReqUrl(String customIp) async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    pref.setString("serverip", customIp);

    _reqUrl = customIp;

    notifyListeners();
  }
}

// ==== 提供 theme ====
ChangeNotifierProvider<ThemeStore> themeProvider = ChangeNotifierProvider((ref) => ThemeStore(ref));
class ThemeStore extends ChangeNotifier{
  bool isDarkmode;

  ThemeData themeData;
  ThemeMode themeMode;

  ProviderReference ref;
  SharedPreferences pref;
  
  ThemeStore(this.ref){
    pref = ref.watch(sharedPreferencesProvider);
    
    if (pref.getString("isDarkmode") == null){
      // 第一次使用，沒有設定值
      isDarkmode = false;
    } else {
      // 有設定值
      isDarkmode = pref.getString("isDarkmode") == "true" ? true : false;
    }

    darkModeSwitch();

    // 初始化也需要使用notifyListeners() 通知讀取初始值的引用們進行更新
    notifyListeners();
  }

  void darkModeSwitch() async{
    switch(isDarkmode){
      case false:
        themeData = ThemeData(brightness: Brightness.light);
        themeMode = ThemeMode.light;
        break;
      case true:
        themeData = ThemeData(brightness: Brightness.dark);
        themeMode = ThemeMode.dark;
        break;
    }
    
    pref.setString("isDarkmode", isDarkmode.toString());

    notifyListeners();
  }
}