@echo off

cd %~dp0

rmdir /S /Q build node_modules
del /F /S /Q package-lock.json *.png