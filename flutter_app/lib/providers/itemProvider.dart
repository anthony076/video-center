
import 'package:flutter/material.dart';

class ItemStore extends ChangeNotifier {
  String id;
  String videoName;
  bool isSelected=false;
  
  ItemStore({this.id, this.videoName});

  void setSelected(bool state){
    isSelected = state;
    notifyListeners();
  }
}
