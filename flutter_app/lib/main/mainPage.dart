import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
//import 'package:flutter_app/providers/downloadProvider.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'package:flutter_app/providers/commonProviders.dart';
import 'package:flutter_app/providers/mainProviders.dart';
import 'package:flutter_app/providers/listProvider.dart';
import 'package:flutter_app/providers/itemProvider.dart';

import 'package:flutter_app/select/selectPage.dart';
import 'package:flutter_app/video/listPage.dart';
import 'package:flutter_app/download/downloadPage.dart';

// ! BUG，在手機端，點擊移動到新資料夾的輸入框，彈出的輸入鍵盤，會造成畫面錯誤
// https://stackoverflow.com/questions/51498911/flutter-keyboard-overflow-with-my-dialog
// https://medium.com/zipper-studios/the-keyboard-causes-the-bottom-overflowed-error-5da150a1c660
// 方法一，使用 resizeToAvoidBottomInset，方法二，使用 singlescrollview，方法三，限制上方組件的大小

class MainPage extends ConsumerWidget {
  final List<Widget> pages = [SelectPage(), ListPage(), DownloadPage()];

  // ===== 清理選擇模式 ====
  void cleanSelect(BuildContext context) {
    ListStore listStore = context.read(listStoreProvider);

    // 將影片反白取消
    for (ItemStore item in listStore.seletedItems) {
      item.setSelected(false);
    }

    // 清除選擇項目
    listStore.seletedItems = [];

    // 關閉選擇模式
    context.read(isSelectModeProvider).state = false;
  }

  // ===== 建構 AppBar ====
  AppBar buildAppBar(BuildContext context) {
    AppBar appBar;

    _showMaterialDialog() {
      List<String> folders = context.read(listStoreProvider).latestFolders;
      ListStore listStore = context.read(listStoreProvider);
      final myController = TextEditingController();

      _printLatestValue() {
        context.read(moveToProvider).state = myController.text;
      }

      myController.addListener(_printLatestValue);

      showDialog(
          context: context,
          barrierDismissible: false,
          builder: (_) => new AlertDialog(
                key: Key("movDialog"),
                title: new Text("移動到"),
                content: SingleChildScrollView(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Container(
                        height: 100.0,
                        width: 300.0,
                        child: ListView.builder(
                            itemCount: folders.length,
                            itemBuilder: (context, index) => Consumer(
                                    builder: (BuildContext context, watch, child) {
                                  watch(moveToProvider).state;

                                  return RadioListTile<String>(
                                    title: Text(folders[index]),
                                    // radio 子項目顯示的內容
                                    value: folders[index],
                                    // 最新選擇的項目
                                    groupValue: context.read(moveToProvider).state,
                                    onChanged: (value) {
                                      context.read(moveToProvider).state = value;
                                    },
                                  );
                                })),
                      ),
                    Text("新增:"),
                    TextField(
                      key: Key("input-new"),
                      controller: myController,
                    )]
                  )
                ),
                actions: <Widget>[
                  TextButton(
                    child: Text('確定'),
                    onPressed: () {
                      // ==== 移動到新資料夾 ====
                      // 取得新資料夾名稱
                      String curSelect = context.read(moveToProvider).state;

                      // 建立新資料夾
                      if (folders.contains(curSelect) == false) {
                        //String reqUrl = context.read(reqUrlProvider);
                        String reqUrl = context.read(urlProvider).reqUrl;

                        Dio dio = context.read(diolProvider(10000));
                        dio.get(reqUrl + "/mf?name=$curSelect");

                        // 重新取回資料夾列表
                        listStore.fetchFolder();
                      }

                      listStore.moveItems();
                      cleanSelect(context);
                      Navigator.of(context).pop();
                    },
                  ),
                  TextButton(
                    child: Text('取消'),
                    onPressed: () {
                      cleanSelect(context);
                      Navigator.of(context).pop();
                    },
                  )
                ],
              ));
    }

    bool isSelectMode = context.read(isSelectModeProvider).state;
    ListStore listStore = context.read(listStoreProvider);

    switch (isSelectMode) {
      case true:
        appBar = AppBar(
          title: Text(listStore.curFolder),
          actions: <Widget>[
            TextButton(
              child: Text("移動", style: TextStyle(color: Colors.white)),
              // ==== 按下移動後，顯示資料夾選擇的對話框 ===
              onPressed: _showMaterialDialog,
            ),
            TextButton(
              child: Text("刪除", style: TextStyle(color: Colors.white)),
              // ==== 按下刪除後，清空 selected 列表，並重建列表 ===
              onPressed: () {
                // 發出 delete req，並重新執行 fetch()，fetch 會更新 listItems
                listStore.deletItems();

                // 清空seletedItems + 關閉選擇模式
                listStore.seletedItems = [];
                context.read(isSelectModeProvider).state = false;
              },
            ),
            TextButton(
              child: Text("取消", style: TextStyle(color: Colors.white)),
              onPressed: () => cleanSelect(context),
            )
          ],
        );
        break;
      case false:
        appBar = AppBar(
          title: Text(listStore.curFolder),
        );
        break;
    }

    return appBar;
  }

  @override
  Widget build(BuildContext context, ScopedReader watch) {
    // 必要，讓 MainPage 可以在 isSelectMode 變化時進行 rebuild
    watch(isSelectModeProvider).state;

    int pageIndex = watch(pageIndexProvider).state;

    return Scaffold(
      appBar: buildAppBar(context),

      // 顯示頁面內容
      body: pages[pageIndex],

      // 定義底部導航列的顯示內容
      bottomNavigationBar: BottomNavigationBar(
        key: Key("bottom"),
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.folder),
            label: "目錄",
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.playlist_play),
            label: "影片",
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.cloud_download),
            label: "下載",
          )
        ],

        // 定義底部導航列的點擊事件
        onTap: (int index) {
          // 切換頁面前，清除選擇模式
          bool isSelectMode = context.read(isSelectModeProvider).state;
          if (isSelectMode) cleanSelect(context);

          context.read(pageIndexProvider).state = index;
        },

        // 儲存 index 的變數
        currentIndex: pageIndex,
      ),
    );
  }
}
