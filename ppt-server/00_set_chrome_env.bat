@echo off

set chrome="C:\Program Files\Google\Chrome\Application\chrome.exe"

setx /m PUPPETEER_EXECUTABLE_PATH %chrome%
setx /m PUPPETEER_SKIP_CHROMIUM_DOWNLOAD true

pause