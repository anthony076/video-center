import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:integration_test/integration_test.dart';

import 'package:flutter_app/main.dart' as app;

void main() {
  IntegrationTestWidgetsFlutterBinding.ensureInitialized();

  testWidgets("offline-server test", (WidgetTester tester) async {
    final appFinder = find.byKey(Key("app"));
    final offlineText = find.text("Server is Offline");
    final checkStatueText = find.text("正在檢查伺服器狀態 ...");
    final reCheckButton = find.text("重新取得狀態");

    // 因為使用的 sailor-route，在建立 MyApp Widget 前，
    // 需要在 main() 對 sailor-router 進行初始化，因此需要執行 main()
    app.main();

    // sailor-route 初始化後，才建立 MyApp Widget
    await tester.pumpWidget(app.MyApp());

    // 檢測 Server 狀態中，畫面顯示 checkStatueFinder
    expect(appFinder, findsOneWidget);
    expect(checkStatueText, findsOneWidget);
    expect(offlineText, findsNothing);

    // 等待15秒後，畫面才會顯示 offlineFinder
    await tester.pump(new Duration(seconds: 15));
    expect(offlineText, findsOneWidget);
    expect(reCheckButton, findsOneWidget);

    // 執行 server 後，點擊畫面中的 重新取得狀態鈕
    // [Issue] 機器是在手機上執行，手機是 android 的環境，無法啟動本地端的 local-server
    // 應該將離線狀態和上線狀態分開測試

    // Process.start(
    //     "python", ["C:\\Users\\Administrator\\Desktop\\server_0307\\server.py"],
    //     runInShell: true, mode: ProcessStartMode.detached);
  });
}
