
import fastify from 'fastify'
import { hpjav } from "./lib/sites";

const server = fastify({
  logger:false,
})

// =====================
// Web-Api
// =====================

server.get('/ping', async (req, resp) => {
  return "pong"
})

// google，http://127.0.0.1:5567/hpjav/aHR0cHM6Ly93d3cuZ29vZ2xlLmNvbS50dw==
// starts-176，https://hpjav.tv/117954/stars-176
//    http://127.0.0.1:5567/hpjav/aHR0cHM6Ly9ocGphdi50di8xMTc5NTQvc3RhcnMtMTc2
// zex-3399c，https://hpjav.tv/tw/168044/zex-399c
//    http://127.0.0.1:5567/hpjav/aHR0cHM6Ly9ocGphdi50di90dy8xNjgwNDQvemV4LTM5OWM=
// ssis-022，https://hpjav.tv/169012/ssis-022
//    http://127.0.0.1:5567/hpjav/aHR0cHM6Ly9ocGphdi50di8xNjkwMTIvc3Npcy0wMjI=
server.get('/hpjav/:url', async (req, resp) => {
  let url:string = Buffer.from(req.params["url"], 'base64').toString()
  console.log(`parsing [hpjav] ${url}`)

  if (url){
    // make sure is part of hpjav
    if (!url.match(/https:\/\/hpjav\.tv\/.*/)){
      return JSON.stringify({"msg":"Not target site"})
    }

    try {
      // start parse
      let result = await hpjav(url)
      return JSON.stringify({"msg":result})
    } catch (e) {
      // err during parse
      console.log(e)
      return JSON.stringify({"msg":"Fail"})
    }

  } else {
    // no url provide
    return JSON.stringify({"msg":"Parameter not found"})
  }
  
})

// =====================
// Start-server
// =====================

server.listen(5567, "0.0.0.0", (err, address) => {
  if (err) {
    console.error(err)
    process.exit(1)
  }
  console.log(`Server listening at http://127.0.0.1:5567/ping`)
})

