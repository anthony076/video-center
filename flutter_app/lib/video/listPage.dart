import 'package:flutter/material.dart';

import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'package:flutter_app/video/itemView.dart' show Item;
import 'package:flutter_app/providers/listProvider.dart';
import 'package:flutter_app/providers/itemProvider.dart';

class ListPage extends ConsumerWidget {

  @override
  Widget build(BuildContext context, ScopedReader watch){
    
    // 數據取用流程，
    //    step1，在 listView 中取得 List<ChangeNotifierProvider<ItemStore>> 的 listStore.listItems
    //    step2，建立子項目時不使用 listItems[index] 建立 Item() 實例，只是將 index 傳遞給 Item 實例
    //    step3，在 Item() 實例 中，
    //           3-1，透過 listStoreProvider 取得 listItems，listItems 保存的是 ChangeNotifierProvider 的實例
    //           3-2，利用 step2 傳入的 index，取得子項目內容的 provider ( itemStoreProvider = listItems[index]; )
    //           3-3，透過子項目自身的 provider，取得子項目的實際內容
    ListStore listStore = watch(listStoreProvider);
    List<ChangeNotifierProvider<ItemStore>> listItems = listStore.listItems;

    return listItems == null
      ? Center(child: CircularProgressIndicator())
      : RefreshIndicator ( 
          onRefresh: listStore.fetchList,
          child: ListView.separated(
            key: Key("list"),
            separatorBuilder: (context, index) => Divider(
              color: Colors.grey,
            ),
            itemCount: listItems.length,
            itemBuilder: (context, index) {
              ValueKey key = ValueKey(listItems[index]);
              return Item(key: key, index: index);
            },
          )
      );
  }
}