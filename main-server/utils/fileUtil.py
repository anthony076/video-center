import os
import shutil
from pathlib import Path
from aiohttp import web
from typing import Union, Optional
from multidict import MultiDictProxy

from utils.db import db
from utils.download import manager
from utils.logger import loggers
from config.const import Const
from config.config import setting

fileUtilLogger = loggers["fileUtil"].createLogger()

def playVideo(md5: str) -> Union[web.FileResponse, web.Response]:
    if db.isExisted(md5):
        filePath = db.getFile(md5)["full"]
        return web.FileResponse(filePath)

    return web.json_response(Const.JSON_FILE_MISS)

def delOneVideo(md5) -> web.Response:
    """
    刪除單一影片
    """
    # 檔案存在
    if db.isExisted(md5):
        filePath: str = db.getFile(md5)["full"]

        # 刪除檔案
        try:
            os.remove(filePath)
        except Exception:
            return web.json_response(Const.JSON_FILE_DEL_ERR)

        # 刪除 db 紀錄
        if db.deleteVideo(md5):
            return web.json_response(Const.JSON_FILE_SUCCESS)
        else:
            return web.json_response(Const.JSON_DB_UPDATE_ERR)

    # 檔案不存在
    else:
        return web.json_response(Const.JSON_FILE_MISS)

def delAllVideo(md5list: list) -> web.Response:
    """
    刪除多個影片
    """
    result = Const.JSON_FILE_DELETE_ALL()
    filePathList = []

    # 取得request檔案路徑
    for md5 in md5list:
        # 取得檔案路徑
        info: Optional[dict] = db.getFile(md5)

        # 只處理db中有紀錄的檔案
        if info:
            filePath: str = info["full"]
            filePathList.append(filePath)

            try:
                # 刪除檔案
                os.remove(filePath)
                # 刪除紀錄
                db.deleteVideo(md5)
                result["deleted"].append(md5)

            except Exception:
                result["fail"].append(md5)

    # 判斷狀態，只有所有要求的檔案都被刪除才會 Success，否則返回 Fail
    if len(result["fail"]) > 0:
        result["status"] = "Fail"
    else:
        result["status"] = "Success"

    return web.json_response(result)

def moveVideo(md5: str, dst):

    info: Optional[dict] = db.getFile(md5)

    if not info:
        return Const.JSON_FILE_MISS

    fileName: str = info["name"]
    srcPath: str = info["path"]

    if dst != "videos":
        distPath: str = setting.videoFolderName + f"\\{dst}"
    else:
        distPath: str = setting.videoFolderName

    src = Path(srcPath) / fileName
    dist = Path(distPath) / fileName

    # 移動檔案
    try:
        shutil.move(src, dist)
    except Exception:
        return Const.JSON_FILE_MOV_ERR

    # 更新DB紀錄
    try:
        db.updataPath(md5, distPath)
    except Exception:
        return Const.JSON_DB_UPDATE_ERR

    return Const.JSON_FILE_SUCCESS

def moveVideos(md5List: list, dst) -> web.Response:
    resp = {}

    for md5 in md5List:
        resp[md5] = moveVideo(md5=md5, dst=dst)

    return web.json_response(resp)

def downloadVideo(params: MultiDictProxy):
    
    tid = manager.addTask(params)

    resp = Const.JSON_Download_ADDTASK
    resp["tid"] = tid

    return web.json_response(resp)

def downloadStatus() -> web.Response:
    result = manager.checkTaskStatus()
    fileUtilLogger.info(f"result={result}")
    
    return web.json_response(result)

def makeFolder(name: str) -> web.Response:
    """
    只允許建立單層資料夾，預設放在 video 目錄下
    """
    newFolderPath = setting.videoFolderName + f"/{name}"

    if not os.path.isdir(newFolderPath):
        try:
            os.mkdir(newFolderPath)
            return web.json_response(Const.JSON_FD_SUCCESS)
        except Exception:
            return web.json_response(Const.JSON_FD_CREATE_ERR)

    return web.json_response(Const.JSON_FD_SUCCESS)

def listFolder() -> web.Response:
    result = Const.JSON_FD_LIST()

    try:
        result["status"] = Const.STATUS_SUCCESS
        result["folders"] = [item.name for item in os.scandir(setting.videoFolderName) if item.is_dir()]
        result["folders"].insert(0, "videos")

    except Exception as e:
        result["status"] = Const.STATUS_FAIL
        result["msg"] = e.__class__.__name__

    return web.json_response(result)

def scanFolder() -> web.Response:
    db.scan()
    return web.json_response(Const.JSON_SCAN_DONE)

def renameFile(oldPath: str, newPath: str) -> web.Response:
    try:
        os.rename(oldPath, newPath)
        return web.json_response(Const.JSON_RENAME_DONE)

    except Exception as e:
        fileUtilLogger.info(f"{e}")
        return web.json_response(Const.JSON_RENAME_Err)