import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

// Root Widget
final appFinder = find.byKey(Key("app"));
final delBtn = find.text("刪除");
final movBtn = find.text("移動");
final movDialog = find.byKey(Key("movDialog"));
final inputField = find.byKey(Key("input-new"));
final dilogEnterBtn = find.text("確定");

// 底部導航列的 widgets
final bottomNaviFinder = find.byKey(Key("bottom"));

final selectIcon = find.byIcon(Icons.folder);
final videoFolder = find.byKey(Key("folder-videos"));

final listIcon = find.byIcon(Icons.playlist_play);
final listFinder = find.byKey(Key("list"));
// 刪除測試用，买了个“过期七年”的东西，脾气直接就上来了-rUgvPq4aZx4
final listItemA = find.byKey(Key("item-e72bf6080df799ac824811afb346dac1"));
// 播放測試/移動測試用，来啦来啦 emoji神还原-6CMHIb9NIq8.mp4
final listItemB = find.byKey(Key("item-97430f319fb9638218bc59b36d473060"));

final downloadIcon = find.byIcon(Icons.cloud_download);
final downloadContent = find.text("This is DownloadPage Content");

final playerFinder = find.byKey(Key("player"));
final backIcon = find.byIcon(Icons.arrow_back_ios);
