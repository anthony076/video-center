import 'package:flutter/material.dart';
import 'package:flutter_app/router.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'package:flutter_app/utils.dart';
import 'package:flutter_app/providers/checkProviders.dart';

class ErrorPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Error"),
        actions: <Widget>[
          Padding(
              padding: EdgeInsets.only(right: 20.0),
              child: GestureDetector(
                onTap: () => Routes.sailor("/setting"),
                child: Icon(Icons.more_vert),
              )),
        ],
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              '未偵測到雲服務器',
              style: msgStyle("red", 26),
            ),
            SizedBox(height: 30),
            Text(
              '請點擊右上角, 設定雲服務器的 IP',
              style:msgStyle("black", 16) 
            ),
            SizedBox(height: 30),
            ElevatedButton(
                child: Text(
                  '重新取得狀態',
                  style: msgStyle("white", 20),
                ),
                onPressed: () {
                  context.read(checkStoreProvider).fetchStatus();
                })
          ],
        ),
      ),
    );
  }
}
