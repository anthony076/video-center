# from asyncio.events import AbstractEventLoop
from enum import Enum
from uuid import uuid1

import asyncio
from multidict import MultiDictProxy

from urlparser.abc import ParserBase
from urlparser.y2b import Youtube
from urlparser.bili import Bili
from urlparser.gimy import Gimy

from utils.db import db
from utils.logger import loggers

downloadLogger = loggers["download"].createLogger()

# ==========
# Task
# ==========
class TaskStatus(Enum):
    # running status
    RUN = "run"
    STOP = "stop"
    # result status
    DONE = "done"
    ERR = "err"

class Task:
    """
    statuscode = 0，download finished
    statuscode = 80，download ongoing
    statuscode = 90，download fail
    statuscode = 98，subprocess error
    statuscode = 99，url error
    """
    def __init__(self, parser: ParserBase, params: MultiDictProxy):
        self.tid: str = uuid1().hex
        self.parser = parser
        self.url = params["u"]
        self.params = params
        self.tstatus: TaskStatus = TaskStatus.STOP

    async def run(self):
        self.tstatus = TaskStatus.RUN

        statusCode, stdout, stderr = await self.parser.download(self.params)
        db.scan()

        if statusCode == 0:
            downloadLogger.info(f"task-{self.tid} done")
            self.tstatus = TaskStatus.DONE
        elif statusCode == 80:
            downloadLogger.info(f"task-{self.tid} ongoing")
            self.tstatus = TaskStatus.RUN
        elif statusCode == 90:
            downloadLogger.info(f"task-{self.tid} page not support")
            self.tstatus = TaskStatus.ERR
        else:
            downloadLogger.info(f"task-{self.tid} err, {stdout}")
            self.tstatus = TaskStatus.ERR


# ==============
# Worker
# 注意，worker 是在 main.py 中啟動，不是在 DownloadManager() 中啟動
# 在 DownloadManager() 中啟動會阻塞 api 的調用
# ==============

# 定義 worker 的工作內容
#   啟動無窮迴圈的 task-worker
#   task-worker 會監聽 DownloadManager 中的 queue，一有任務就立即執行下載
async def worker(id: int) -> None:
    while True:
        # 監聽 queue 是否為空
        if not DownloadManager._queue.empty():
            # 取得任務
            task: Task = DownloadManager._queue.get_nowait()
            # 執行任務
            await asyncio.create_task(task.run())
        else:
            # 無任務時進行等待，不關閉 worker
            await asyncio.sleep(1)

# 建立 worker
async def startWorker() -> None:
    taskWorkers = [asyncio.create_task(worker(i)) for i in range(5)]
    for taskWorker in taskWorkers:
        await taskWorker

# ==============
# 下載管理，用於提供下載相關的 api 給外部
# 例如，建立下載、查詢下載狀態
# ==============
class DownloadManager:
    _queue: asyncio.Queue = asyncio.Queue()

    # 用於快速取得 task 實例，方便查詢狀態
    taskMap: dict = {}
    
    def __init__(self):
        pass

    @property
    def isQueueEmpty(self) -> bool:
        return DownloadManager._queue.empty()

    @property
    def ongoing(self) -> int:
        return DownloadManager._queue.qsize()

    def checkTaskStatus(self) -> dict:
        # TODO，下載完畢後，需要清除 taskMap

        status: dict = {}
        taskmap: dict = DownloadManager.taskMap

        for tid in list(taskmap.keys()):
            status[tid] = {}
            status[tid]["tid"] = taskmap[tid].tid
            status[tid]["status"] = taskmap[tid].tstatus.value

        return status

    def addTask(self, params: MultiDictProxy) -> int:
        url: str = params["u"]

        # 取得 parser
        if "youtube" in url:
            parser: ParserBase = Youtube()
        elif "bilibili" in url:
            parser: ParserBase = Bili()
        elif "gimy" in url:
            parser: ParserBase = Gimy()
        else:
            return ValueError("unsupported url")

        # 建立任務
        task = Task(parser, params)
        DownloadManager.taskMap[task.tid] = task

        # 添加任務
        DownloadManager._queue.put_nowait(task)

        return task.tid

manager = DownloadManager()
