import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'package:flutter_app/providers/checkProviders.dart';
import 'package:flutter_app/main/MainPage.dart';
import 'package:flutter_app/check/errorPage.dart';
import 'package:flutter_app/check/loadPage.dart';

class CheckPage extends StatelessWidget {
  CheckPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer(
      builder: (context, watch, child) {
        AsyncValue<bool> future = watch(checkStoreProvider).isServed;

        return future.when(
          data: (_) => MainPage(),
          loading: () => LoadPage(),
          error: (err, stack) => ErrorPage(),
        );
      },
    );
  }
}
