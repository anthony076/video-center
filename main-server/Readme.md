y2b 支援 (pc+mobile) 列表下載，支援列表選擇下載
bili  (pc+mobile) 支援列表下載，支援列表選擇下載
gimy  (pc) 支援列表下載，不支援咧表選擇下載

## Issues
- [bili] 20210529，bili 網站更新，annie v0.10.3 下載失效，但 annie v0.9.6正常

## TODOS
- 瀏覽器插件

- [bili.py][genArgs] 加入對參數的驗證，避免參數不正確而報錯
- [bili.py][downlaod] 對 bili-space-url 加入篩選影片功能，可選擇要下載的集數
- [bili.py] 將下載工具 annie 改為 youtubedl, annie 更新速度太慢

- [gimy.py][_epPageUrls] 
    將 _epPageUrls 改為異步併發，增加解析的速度，否則解析期間會阻塞 server 運行

- [gimy.py][_epPageUrls] 
    尋找 epPageUrls 時，直接定位到目標連結，節省過濾時間

- [gimy.py] 透過 curl，提供更新下載狀態的功能
- [gimy.py] 僅支援 gimy.app 戲劇類的下載，不支援 gimytv 戲劇類的下載

- 添加對不支援頁面的錯誤捕捉

