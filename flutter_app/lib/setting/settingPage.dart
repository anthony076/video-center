import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/providers/commonProviders.dart';
import 'package:flutter_app/setting/ipComponent.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

void _setIp(BuildContext context) {
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text(
          '設定 IP',
          style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
        ),
        content: Column(
          mainAxisSize:MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            IpComponent(),
            SizedBox(height: 20),
            Text("若使用AVD，請輸入 10.0.2.2"),
        ]),
        actions: <Widget>[
          TextButton(
            child: Text('確定'),
            onPressed: () {
              Navigator.of(context).pop(); // Dismiss alert dialog
            },
          ),
        ],
      );
    }
  );
}

class SettingPage extends ConsumerWidget {
  const SettingPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context, ScopedReader watch) {
    UrlStore urlStore = watch(urlProvider);
    ThemeStore themeStore = watch(themeProvider);

    return Scaffold(
        appBar: AppBar(
          title: Text("Setting"),
        ),
        body: ListView(children: <Widget>[
          //
          ListTile(
            title: Text('Server IP'),
            subtitle: Text(urlStore.reqUrl),
            trailing: Icon(Icons.keyboard_arrow_right),
            onTap: () {
              _setIp(context);
            },
          ),
          ListTile(
            title: Text('護眼模式'),
            subtitle: themeStore.isDarkmode == true 
              ? Text("開")
              : Text("關"),
            trailing: CupertinoSwitch(
              value: themeStore.isDarkmode,
              onChanged: (value) {
                themeStore.isDarkmode = !themeStore.isDarkmode;
                themeStore.darkModeSwitch();
              },
            ),
          ),
        ]));
  }
}
