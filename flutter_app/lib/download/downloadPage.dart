import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_app/providers/downloadProvider.dart';

class DownloadPage extends ConsumerWidget {
  final TextEditingController epsController = new TextEditingController();
  
  @override
  Widget build(BuildContext context, ScopedReader watch) {
    DownloadStore downloadStore = watch(downloadProvider);
    
    return SafeArea(
      child: Column(
        children: <Widget>[
          // ==== 顯示按鈕控制區 ====
          ButtonBar(
            alignment: MainAxisAlignment.center,
            children: <Widget>[
              // ===== 頁面選擇鈕(dropdown) =====
              Consumer(builder: (BuildContext context, ScopedReader watch, _) {
                String currentSite = watch(downloadProvider).currentDropItem;

                return Center(
                  child: DropdownButton<String>(
                    value: currentSite,
                    
                    // dropdonw 的內容改變
                    onChanged: (String newValue) {
                      //String url = downloadStore.siteMap(newValue);
                      String url = DownloadStore.siteMap[newValue]["url"];

                      downloadStore.webViewController
                          .loadUrl(urlRequest: URLRequest(url: Uri.parse(url)));

                      context.read(downloadProvider).setSite(newValue);
                    },

                    // dropdonw 顯示的內容
                    items: <String>[
                      'youtube',
                      'bilibili',
                      'gimy'
                    ].map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                  ),
                );
              }),
              // ===== 增加間隔 =====
              SizedBox(width: 5),
              // ===== 上一頁鈕 =====
              ElevatedButton(
                child: Icon(Icons.arrow_back),
                onPressed: () {
                  downloadStore.webViewController.goBack();
                },
              ),
              // ===== 下一頁鈕 =====
              ElevatedButton(
                child: Icon(Icons.arrow_forward),
                onPressed: () {
                  downloadStore.webViewController.goForward();
                },
              ),
              // ===== 下載鈕 =====
              ElevatedButton(
                child: Icon(Icons.download_rounded),
                onPressed: () async {
                  // 透過 webview 讀取當前瀏覽頁面的網址
                  Uri curUrl = await context.read(downloadProvider).webViewController.getUrl();

                  // 點擊下載鈕後，顯示 dialog
                  showDialog(
                    context: context,
                    builder: (context) {
                      return new SimpleDialog(
                        title: new Text("下載"),
                          children: <Widget>[
                            ElevatedButton(
                              child: Text('下載影片'),
                              onPressed: () async {
                                await downloadStore.send(url: curUrl.toString(), isList: false);

                                // 送出請求後，在底部顯示訊息
                                final messenger = ScaffoldMessenger.of(context);
                                messenger.showSnackBar(SnackBar(content: Text("已添加到下載列表")));

                                Navigator.pop(context);
                              },
                            ),
                            ElevatedButton(
                              child: Text('下載列表'),
                              onPressed: () async {
                                // 發送下載請求，若下載網址需要經過處理，統一在 downloadStore 中處理
                                await downloadStore.send(url: curUrl.toString(), isList: true, select: epsController.text);

                                // 底部訊息，提示使用者請求已發送
                                final messenger = ScaffoldMessenger.of(context);
                                messenger.showSnackBar(SnackBar(content: Text("已添加到下載列表")));
                                
                                // 清除集數輸入框，避免下次顯示歷史值
                                epsController.text = "";

                                Navigator.pop(context);
                              },

                            ),
                            
                            //TODOs，添加有效字串驗證
                            TextField(
                              enabled: DownloadStore.siteMap[downloadStore.currentDropItem]["supportSelect"],
                              controller: epsController,
                              textAlign: TextAlign.center,
                              decoration: InputDecoration(hintText: '請輸入集數，例如，1-3,4,5', ),
                            ),
                          ],
                      );
                    });
                },
              ),              
            ],
          ),

          // ==== 顯示 webview ====
          Expanded(
            child: Stack(
              children: [
                InAppWebView(
                  key: downloadStore.webViewKey,

                  // 預設網址
                  initialUrlRequest:
                      URLRequest(url: Uri.parse("https://www.youtube.com/")),

                  // 設置 webview
                  initialOptions: downloadStore.webviewOptions,

                  onWebViewCreated: (controller) {
                    context.read(downloadProvider).webViewController =
                        controller;
                  },
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
