peewee==3.14.1
aiohttp==3.7.3
requests2==2.16.0
ujson==4.0.2
robotframework==3.2.2
RESTinstance==1.0.2
wget==3.2
pyquery==1.4.3