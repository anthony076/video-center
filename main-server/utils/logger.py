import logging

class myLogger:
    def __init__(self, name, debug):
        self.name = name
        self.debug = debug
        self.debug_fmt = logging.Formatter('[%(asctime)s][%(funcName)s] %(message)s')
        self.info_fmt = logging.Formatter('[%(asctime)s] %(message)s')

    def createLogger(self):
        newLogger = logging.getLogger(self.name)
        handler = logging.StreamHandler()

        if self.debug:
            newLogger.setLevel(logging.DEBUG)
            handler.setFormatter(self.debug_fmt)
        else:
            newLogger.setLevel(logging.INFO)
            handler.setFormatter(self.info_fmt)
        
        newLogger.addHandler(handler)

        return newLogger

# debug=False，只會印出 info，debug=True，會印出 info 和 debug
# logger.info() 用於一般輸出，logger.debug() 用於debug輸出
loggers = {
    "main": myLogger("main", debug=False),
    "db": myLogger("db", debug=False),
    "download": myLogger("db", debug=False),
    "fileUtil": myLogger("fileUtil", debug=False),
    "bili": myLogger("bili", debug=False),
    "gimy": myLogger("gimy", debug=True),
    "y2b": myLogger("y2b", debug=False),
}