import 'package:flutter/material.dart';
import 'package:flutter_app/providers/commonProviders.dart';
import 'package:flutter_app/providers/listProvider.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:url_launcher/url_launcher.dart';

import 'package:flutter_app/router.dart';
import 'package:flutter_app/providers/mainProviders.dart';
import 'package:flutter_app/providers/itemProvider.dart';

_launchURL(String url) async {
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not launch $url';
  }
}

// =========================
// Item，顯示 ListView 的子項目
// 透過 provider 取得 listitems 和 傳入的 index 
//    -> 透過 listitems[index] 取得 ItemProvider 
//    -> 透過 ItemProvider 取得 ItemStore 
//    -> 透過 ItemStore 取得數據
// =========================

class Item extends ConsumerWidget {
  final ValueKey key;
  final int index;

  Item({this.key, this.index});

  @override
  Widget build(BuildContext context, ScopedReader watch) {
    bool isSelectMode = context.read(isSelectModeProvider).state;

    // 取得 listItems
    ListStore listStore = context.read(listStoreProvider);
    List<ChangeNotifierProvider<ItemStore>> listItems = context.read(listStoreProvider).listItems;

    // 從 listItems[index] 取得當前 Item 自身的 itemProvider
    // 並透過 itemProvider 取得 itemStore 
    ChangeNotifierProvider<ItemStore> itemStoreProvider = listItems[index];
    ItemStore itemStore = watch(itemStoreProvider);

    Color bgColor;
    switch (itemStore.isSelected) {
      case true:
        bgColor = Colors.blue[100];
        break;
      case false:
        bgColor = Colors.white;
        break;
    }

    return ListTile(
      key: Key("item-${itemStore.id}"),
      title: Text(itemStore.videoName),
      tileColor: bgColor,
      // ==== 點擊播放視頻或選擇影片 ====
      onTap: () {
        bool isSelectMode = context.read(isSelectModeProvider).state;

        if (isSelectMode) {
          // ==== 選擇模式下，單點就可選擇影片，不需要長按 ====
          listStore.addSelected(itemStore);
          itemStore.isSelected = !itemStore.isSelected;
        } else {
          // ==== 非選擇模式下，單點播放影片 ====
          bool isWeb = context.read(isWebProvider);
          String reqUrl = context.read(urlProvider).reqUrl;

          if (isWeb) {
            _launchURL(reqUrl + "/v?id=${itemStore.id}");
          } else {
            Routes.sailor.navigate("/player",
                params: {"url": reqUrl + "/v?id=${itemStore.id}"});
          }
        }
      },
      // ==== 長按進入選擇模式 ====
      onLongPress: () {
        // 只有第一次長按會使 isSelectMode=true
        if (isSelectMode == false) {
          context.read(isSelectModeProvider).state = true;
        }

        listStore.addSelected(itemStore);
        itemStore.isSelected = !itemStore.isSelected;
      },
    );
  }
}
