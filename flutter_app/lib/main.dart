import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_app/router.dart';
import 'package:flutter_app/providers/commonProviders.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() async {
  // for flutter_inappwebview
  // it should be the first line in main method
  WidgetsFlutterBinding.ensureInitialized();

  if (Platform.isAndroid) {
    await AndroidInAppWebViewController.setWebContentsDebuggingEnabled(true);
  }

  Routes.createRoutes();

  final sharedPreferences = await SharedPreferences.getInstance();
  
  runApp(ProviderScope(
    overrides: [
      sharedPreferencesProvider.overrideWithValue(sharedPreferences)
    ],
    child: MyApp()));
}

class MyApp extends ConsumerWidget {
  @override
  Widget build(BuildContext context, ScopedReader watch) {
    ThemeStore themeStore = watch(themeProvider);
    return MaterialApp(
      key: Key("app"),
      title: 'Video Player',
      theme: themeStore.themeData,
      themeMode: themeStore.themeMode,
      initialRoute: "/check",
      navigatorKey: Routes.sailor.navigatorKey,
      onGenerateRoute: Routes.sailor.generator(),
      debugShowCheckedModeBanner: false,
    );
  }
}
