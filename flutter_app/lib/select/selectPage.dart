import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'package:flutter_app/providers/mainProviders.dart';
import 'package:flutter_app/providers/listProvider.dart';

// !bug，將影片移動到新增資料夾後，切換到目錄頁面，不會顯示新建的資料夾
class SelectPage extends ConsumerWidget {
  @override
  Widget build(BuildContext context, ScopedReader watch) {
    AsyncValue<List<String>> future = watch(listStoreProvider).folders;

    return future.when(
      data: (folders) => GridView.count(
        key: Key("folder-grid"),
        crossAxisCount: 2,
        children: List.generate(
            folders.length,
            (index) => Column(
                  key: ValueKey("folder-${folders[index]}"),
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    // ==== 圖示 ====
                    InkWell(
                      onTap: () {
                        ListStore listStore = context.read(listStoreProvider);

                        listStore.curFolder = folders[index];

                        // 重新刷新影片列表的內容
                        //listStore.fetchList(listStore.curFolder);
                        listStore.fetchList();

                        // 跳轉到影片頁面
                        context.read(pageIndexProvider).state = 1;
                      },
                      // TODOs，設定圖示的大小隨著螢幕大小而定
                      child: Icon(
                        Icons.folder,
                        size: 150,
                        color: Colors.lightBlue,
                      ),
                    ),
                    // ==== 文字 ====
                    Text(
                      folders[index],
                      style: TextStyle(fontSize: 20),
                    )
                  ],
                )),
      ),
      loading: () => CircularProgressIndicator(),
      error: (err, trace) => TextButton(
        child: const Text('重新獲取目錄'),
        onPressed: () {
          ListStore listStore = context.read(listStoreProvider);
          listStore.fetchFolder();
        },
      ),
    );
  }
}
